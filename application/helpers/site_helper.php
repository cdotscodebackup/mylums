<?php
function get_user_meta($user_id, $key = NULL, $single = NULL)
{
	$CI =& get_instance();
	
	$CI->db->where('user_id', $user_id);
	if($key)
	{
		$CI->db->where('meta_key', $key);
	}
	$CI->db->order_by('id','-');
	$table = $CI->db->get('user_meta')->result_array();
	
	$data = array();
	
	$keyChecking = 0;
	
	if(count($table) > 0)
	{
		foreach($table as $row)
		{
			//echo $key;
			//die();
			if($row['meta_key'] == $key)
			{
				$keyChecking = 1;
				return $row['meta_value'];
			} else {
				$data[$row['meta_key']] = $row['meta_value'];
			}
		}
	}
	
	if($key)
	{
		return NULL;
	}
	
	return $data;
}

function update_user_meta($userID, $key = NULL, $value =  NULL)
{
	$thiss =& get_instance();
	
	$thiss->db->where('user_id',$userID);
	$thiss->db->where('meta_key',$key);
	$q = $thiss->db->get('user_meta');
	
	if ( $q->num_rows() > 0 ) 
	{
		$data['meta_value'] = $value; 
		$thiss->db->where('meta_key',$key);
		$thiss->db->where('user_id',$userID);
		$thiss->db->update('user_meta',$data);
	} else {
		
		$data = array(
		'user_id' => $userID,
		'meta_key' => $key,
		'meta_value' => $value
		);
		
		$thiss->db->insert('user_meta', $data);
		return $thiss->db->insert_id();
	}
}

function get_meta($Table,$Where,$ID)
{
	$CI =& get_instance();
	$CI->db->where($Where, $ID);
	$table = $CI->db->get($Table)->result_array();	
	//echo $CI->db->last_query();
	return $table;
}

function array_value_recursive($key, array $arr)
{
    $val = array();
    array_walk_recursive($arr, function($v, $k) use($key, &$val){
        if($k == $key) array_push($val, $v);
    });
    return count($val) > 1 ? $val : array_pop($val);
}

function arrayEncode($data)
{
	$return = serialize($data);
	return $return;
}

function arrayDecode($data)
{
	$return = unserialize($data);
	return $return;
}

function slug($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}