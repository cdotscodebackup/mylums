<?php
class Setting_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
	
	public $Table = 'options';
    public function __construct()
    {
        $this->load->database();
    }
	
	function get($key)
	{
		$this->db->where('option_name',$key);
		$this->db->from('options');
		$data = $this->db->get()->row_array();
		return $data; 
	}
	
	function update($key,$value)
	{
		$data = array();
		$data['option_value'] = $value;
		$this->db->where('option_name',$key);
		$this->db->update($this->Table, $data);
		$report = array();
		$report['error'] = $this->db->error();
		$report['message'] = $this->db->error();
		if($report !== 0){
			return true;
		} else {
			return false;
		}
	
	}
	
	function update_manufacture($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update($this->Table, $data);
		$report = array();
		$report['error'] = $this->db->error();
		$report['message'] = $this->db->error();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}
}
?>