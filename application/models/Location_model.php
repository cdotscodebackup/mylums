<?php
class Location_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
	
    public function __construct()
    {
        $this->load->database();
    }
	
	public function countries()
	{
		$this->db->select('*');
		$this->db->from('countries');
		$query = $this->db->get();
		return $query->result_array(); 	
	}
	
	public function states($country_id)
	{
		$this->db->select('*');
		$this->db->where('country_id', $country_id);
		$this->db->from('states');
		$query = $this->db->get();
		return $query->result_array(); 	
	}
	
	public function cities($state_id)
	{
		$this->db->select('*');
		$this->db->where('state_id', $state_id);
		$this->db->from('cities');
		$query = $this->db->get();
		return $query->result_array(); 	
	}
}

?>