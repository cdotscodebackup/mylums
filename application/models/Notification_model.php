<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model
{
	//protected $table = 'users';
	
	public function signup($data)
	{
		//var_dump($data);
		$config = array('mailtype'  => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
    	$this->load->library('email', $config);
		$this->email->set_header('Content-Type', 'text/html');
		
		$this->email->from('team@lums.com', 'Team Lums');
		$this->email->to($data['email']);
		$this->email->subject('Verify your email address');
		$code = password_encode($data['user_id']);
		$code = urlencode( $code );
		$urlSignUP = base_url() . 'verification/' . $code;
		$message = 'Dear user,<br /><br />
					Thank you for sign up, please <a href="'.$urlSignUP.'">click here</a> to verify your email. And activate your account.<br /><br />					
					if you need any help please email us.<br /><br /><br />					
					Thanks<br />
					Team Lums';
		$this->email->message($message);		
		return $this->email->send();
	}
	
	public function signup_thanks($userData)
	{
		//var_dump($userData);
		
		$config = array('mailtype'  => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
    	$this->load->library('email', $config);
		$this->email->set_header('Content-Type', 'text/html');
		
		$this->email->from('team@lums.com', 'Team Lums');
		$this->email->to($userData['email']);
		$this->email->subject('Thanks your account activated');
		$code = password_encode($data['user_id']);
		$code = urlencode( $code );
		$urlSignUP = base_url() . 'verification/' . $code;
		$message = 'Dear user,<br /><br />
					Thank you for verification, please login and update your profile on mobile App.<br /><br />					
					if you need any help please email us.<br /><br /><br />					
					Thanks<br />
					Team Lums';
		$this->email->message($message);		
		return $this->email->send();
	}
	
	public function reset_password($userEmail,$newPassword)
	{
		$config = array('mailtype'  => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
    	$this->load->library('email', $config);
		$this->email->set_header('Content-Type', 'text/html');
		
		$this->email->from('team@lums.com', 'Team Lums');
		$this->email->to($userEmail);
		$this->email->subject('Thanks your password updated');
		//$code = password_encode($data['user_id']);
		//$code = urlencode( $code );
		//$urlSignUP = base_url() . 'verification/' . $code;
		$message = 'Dear user,<br /><br />
					Your password has been changed successfully!<br /><br />
					your new password is  <strong>' . $newPassword . '</strong><br /><br />
					if you need any help please email us.<br /><br /><br />					
					Thanks<br />
					Team Lums';
		$this->email->message($message);		
		return $this->email->send();
	}
	
	public function help_desk($data)
	{
		$config = array('mailtype'  => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
    	$this->load->library('email', $config);
		$this->email->set_header('Content-Type', 'text/html');
		
		$this->email->from('team@lums.com', 'Team Lums');
		$this->email->subject('Help Desk');
		$this->email->from($data['email'], $data['first_name'] . ' ' . $data['last_name']);
		$this->email->to('nadeem@creative-dots.com');
			
			
			$message = $data['message'] .'<br /><br />
					Name: <strong>' . $data['first_name'] . ' ' . $data['last_name'] . '</strong><br />
					Email: <strong>' . $data['email'] . '</strong><br />
					Roll Number: <strong>' . $data['roll_number'] . '</strong><br />
					Phone Number: <strong>' . $data['phone_number'] . '</strong><br /><br /><br />				
					Thanks<br />
					Team Lums';
		$this->email->message($message);
		return $this->email->send();
	}
	
	public function massemailing($to,$subject,$message)
	{
		$config = array('mailtype'  => 'html', 'charset' => 'utf-8', 'wordwrap' => TRUE);
    	$this->load->library('email', $config);
		$this->email->set_header('Content-Type', 'text/html');
		
		$this->email->from('team@lums.com', 'Team Lums');
		$this->email->subject($subject);
		$this->email->to($to);
		$message = $message;
		$this->email->message($message);
		return $this->email->send();
	}
	
	public function sendFCM($message, $id, $type='', $type_id ='',$user = NULL) 
	{
		
		$API_ACCESS_KEY = "AAAAFDHCMHA:APA91bEw1c9O62X7Z_fB36Fyu4E45T8FJsrHr-2ev-G1w1ELSw5LCTz_e1JWyAfH72he1tBi0otT1pOYsr52FP6LiUEaATdD98tOWiCASrlnhhipNIc5f99VMqfb8zNVli4malycH0p4";
		
		$url = 'https://fcm.googleapis.com/fcm/send';
		
		$fields = array ('registration_ids' => array ($id),
													  'data' => array (
																			"message" => $message,
																			'type' => $type,
																			'type_id'=>$type_id,
																			'user' => $user),
													  'priority' => 'high',
		'notification' => array('title' => $message['title'], 'body' => $message['body'],),);
		
		$fields = json_encode ( $fields );
		
		$headers = array ('Authorization: key=' . $API_ACCESS_KEY, 'Content-Type: application/json');
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		
		return $result;
	}
	
	public function createnotification($receiver_id,$sender_id,$type,$type_id,$status = NULL)
	{
		$insert = array();
		$insert['receiver_id'] = $receiver_id;
		$insert['sender_id'] = $sender_id;
		$insert['type'] = $type;
		$insert['type_id'] = $type_id;
		$insert['status'] = 'unread';
		$insert['created_at'] = time();
		$this->db->insert('notification', $insert);
	}
}
?>