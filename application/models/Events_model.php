<?php
class Events_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
	
	public $Table = 'events';
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_manufacture_by_id($id)
    {
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->from($this->Table);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch manufacturers data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_manufacturers($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*,
						  (SELECT count(*) FROM event_users WHERE event_id = events.id) as members,
						  (SELECT count(*) FROM event_gallery WHERE event_id = events.id) as gallery');
		//$this->db->where('users.user_type != "admin"');
		//$this->db->where('users.id = profile.user_id');
		$this->db->from($this->Table);
		
		if($search_string && $this->input->post('searchField'))
		{
			$searchField = $this->input->post('searchField');
			$this->db->like($searchField, $search_string);
		}
		
		$this->db->group_by('id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('id', $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }
		
		if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_manufacturers($search_string=null, $order=null)
    {
		$this->db->select('*,(SELECT count(*) FROM event_users WHERE event_id = events.id) as members');
		//$this->db->where('users.user_type != "admin"');
		//$this->db->where('users.id = profile.user_id');
		$this->db->from($this->Table);
		
		if($search_string && $this->input->post('searchField'))
		{
			$searchField = $this->input->post('searchField');
			$this->db->like($searchField, $search_string);
		}
		
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('id', 'Asc');
		}
		
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert($this->Table, $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update($this->Table, $data);
		$report = array();
		$report['error'] = $this->db->error();
		$report['message'] = $this->db->error();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $id - manufacture id
    * @return boolean
    */
	function delete_manufacture($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->Table);
		
		//$this->db->where('user_id', $id);
		//$this->db->delete('profile');
	}
	
	function get_gallery($event_id)
	{
		$this->db->select('*');
		$this->db->where('event_id', $event_id);
		$this->db->from('event_gallery');
		$query = $this->db->get();
		return $query->result_array(); 
	}
	
	function add_gallery($data)
    {
		$insert = $this->db->insert('event_gallery', $data);
	    return $insert;
	}
	
	public function get_event_object($event_id)
	{
		$totalCheckedIn = $this->checkedintotal($event_id);
		$interests 		= $this->interests($event_id);
		$gallery 		= $this->media($event_id);
		$mystatus 		= $this->mystatus($event_id);
		$checkin 		= $this->mystatuscheckin($event_id);
		
		$data['data']['date_time'] = date('d-m-Y h:i a', strtotime($data['data']['dates'] . ' ' . $data['data']['start_time']));
		$data['data']['mystatus'] = $mystatus;
		$data['data']['mycheckin'] = $checkin;
		$data['data']['gallery'] = $gallery;
		$data['data']['interests'] = $interests;
		$data['data']['users'] = $this->userstotal($event_id);
		$data['data']['users']['checkedin'] = $totalCheckedIn['users'];
		$data['data']['users']['checkedin-total'] = $totalCheckedIn['total'];
		return $data;
	}
	
}