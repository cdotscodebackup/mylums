<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Industry Api

require('Api.php');

class Industry extends Api 
{
	public $content_type = "application/json";
	public $columns;
	public $columnsEdit;
	
	public function __construct()
    {
        parent::__construct();
	}
	
	public function index()
	{
	
	}
	
	public function all()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/alumni/profile/');
				//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*, CONCAT(\''.$path. '\', profile.image) as imagepath, CONCAT(\''.$profile. '\', users.id) as profilepath');
				$this->db->select('*');
				//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
				//$this->db->where('users.id = profile.user_id');
				//$this->db->where('users.user_type != "admin"');
				//$this->db->where('users.status', "active");
				$this->db->from('industry');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				//var_dump($data['data']);
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Industry list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Industry not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
}
?>