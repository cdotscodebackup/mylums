<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Api.php');

class Device extends Api 
{
	public $content_type = "application/json";
	public function __construct()
    {
        parent::__construct();
		$this->load->model('mobile_model');
	}
	
	public function index()
	{
	
	}
	
	public function registration()
	{
		$userID = $this->currentUserId->id;
		$dataPost = $this->input->post();
		$dataPost['user_id'] = $userID;
		
		$return = $this->mobile_model->register($dataPost);
		
		if($return)
		{
			$response['status'] = 'Successful';
			$response['message'] = 'Device register successfully.';
			$this->api_model->response($this->json->encode($response),200);	
		} else {
			$response['status'] = 'Error';
			$response['message'] = 'Device already registered.';
			$this->api_model->response($this->json->encode($response),200);	
		}
	}
}