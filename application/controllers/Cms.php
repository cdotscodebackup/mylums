<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//return false;
		//$this->load->view('welcome_message');
		$page = $this->uri->segment(2); //$this->uri->segement('3');
		$data = $this->pages($page);
		if($data)
		{
			//var_dump($data);
			$pages= array();
			$pages['title'] = $data['title'];
			$pages['content'] = $data['content']; 
			$this->load->view('cms',$data);
		} else
		{
			show_404();
		}
	}
	
	public function terms_and_conditions()
	{
		$data = array();
		$data['title'] = 'My LUMS: Terms and Conditions';
		$data['content'] = "<p><u>Terms Of Service</u><br />
  In order to provide our Services (as defined below) through My LUMS App we need to obtain your express agreement to our Terms of Service (&quot;Terms&quot;). You agree to our Terms by registering, installing, accessing, or using our My LUMS App.  <a name=\"terms-of-service-our-services\" id=\"terms-of-service-our-services\"></a><br />
  <u>Our Services</u> <br />
  <strong><u>Privacy And Security Principles</u></strong><br />
  &nbsp;Since we started My LUMS, we've built our Services with strong privacy and security principles in mind.</p>
<ul>
  <li><strong>Connecting You With Other People</strong>.&nbsp;We provide ways for you to connect and communicate with other My LUMS users including through messages and sharing your location when you choose. My LUMS App works with partners, service providers, and affiliated companies to help us provide ways for you to connect with their services. We use the information we receive from them to help operate, provide, and improve our Services.</li>
  <li><strong>Ways To Improve Our Services</strong>.&nbsp;We analyze how you make use of My LUMS App, in order to improve all aspects of our services described here. </li>
  <li><strong>Information About Businesses</strong>.&nbsp;We provide information about third party businesses or services to you through notifications. The My LUMS App may contain offers for something that might interest you. We do not want you to have a spam experience; as with all of your messages, you can manage these communications, and we will honor the choices you make.</li>
  <li><strong>Safety And Security.</strong><strong>&nbsp;</strong>We work to protect the safety and security of My LUMS by appropriately dealing with abusive people and activity and violations of our Terms. We prohibit misuse of our services, harmful conduct towards others, violations of our Terms and policies, and address situations where we may be able to help support or protect our community. We develop automated systems to improve our ability to detect and remove abusive people and activity that may harm our community and the safety and security of our Services. If we learn of people or activities like this, we will take appropriate action by removing such people or activity or contacting law enforcement. </li>
  <li><strong>Enabling Global Access To Our Services.</strong><strong>&nbsp;</strong>To operate our global Service, we need to store and distribute content and information in data centers and systems around the world, including outside your country of residence. This infrastructure may be owned or operated by our service providers or affiliated companies.</li>
</ul>
<p><a name=\"terms-of-service-about-our-services\" id=\"terms-of-service-about-our-services\"></a><strong><u>About Our Services</u></strong><br />
  <strong>Registration.</strong><strong>&nbsp;</strong>You must register for our services using accurate information, provide your current mobile phone number, and, if you change it, update your mobile phone number using our in-app change number feature. You agree to receive text messages and phone calls (from us or our third-party providers) with codes to register for our Services.<br />
  <a name=\"terms-of-service-age\" id=\"terms-of-service-age\"></a><strong>Age.</strong><strong>&nbsp;</strong>If you live in Pakistan and are a part of Lahore University Of Management Sciences, you must be at least 18 years old to use our services or such greater age required in your country to register for or use our services.<br />
  <strong>Devices And Software.</strong><strong>&nbsp;</strong>You must provide certain devices, software, and data connections to use our services, which we otherwise do not supply. In order to use our services, you consent to manually or automatically download and install updates to our services. You also consent to our sending you notifications via My LUMS App from time to time, as necessary to provide our services to you.<br />
  <strong>Fees And Taxes.</strong><strong>&nbsp;</strong>You are responsible for all carrier data plans, Internet fees, and other fees and taxes associated with your use of our services.</p>";
		$this->load->view('cms.php',$data);
	}
	
	public function privacy_policy()
	{
		$data = array();
		$data['title'] = 'MY LUMS PRIVACY POLICY';
		$data['content'] = "<p>At the Lahore University Of Management Sciences, our relationship with our alumni, parents, donors and friends is important to us. Collecting and maintaining personal information about you allows us to better communicate with you and engage you with the University in meaningful ways.<br />
  We are committed to respecting and safeguarding your privacy. With that in mind, we've also included a <strong>Privacy Notice</strong> to clarify what data we collect, why we collect it, and how we use it.</p>
<p><strong><u>My LUMS PRIVACY POLICY</u></strong><br />
  The My LUMS App is provided by the Lahore University Of Management Sciences and is designed to facilitate communication among alumni for personal and University-related purposes. To safeguard the operation of MY LUMS App, Lahore University of Management Sciences has adopted the following guidelines and policies.</p>
<p><strong><u>GUIDELINES &amp; POLICIES FOR PROPER USE</u></strong><br />
  Information available on the My LUMS app s intended for the private use of LUMS Alumni and for University-related purposes. Users of the My LUMS app must abide by the following rules:</p>
<p><strong>A.</strong> Unauthorized use of information available on the My LUMS App for commercial, political, or business purposes, such as selling products or sending broadcast email, is strictly prohibited.</p>
<p><strong>B.</strong> Use of communications available through the My LUMS App for any commercial, public, or political purposes is strictly prohibited. Prohibited activities include, but are not limited to, solicitations for commercial services and mass mailings for commercial purposes.</p>
<p><strong>C.</strong> Members may download or copy any downloadable materials displayed on the My LUMS App for home, non-commercial, and personal use only and must maintain all copyright, trademark, and other notices contained in such material and they agree to abide by all additional copyright notices or restrictions contained in any material accessed through the My LUMS App.</p>
<p><strong>D.</strong> Members shall not restrict or inhibit any other user from enjoying any service offered through the My LUMS App, and shall not post obscene materials or use abusive, defamatory, profane, or threatening language of any kind. Additionally, users shall not upload, transmit, distribute, or otherwise publish any materials containing a virus or any other harmful component.</p>
<p><strong>E.</strong> All aliases adopted by My LUMS members are subject to approval by the Lahore University of Management Sciences.<br />
  LUMS is not responsible for the content of the information available on, and have no obligation to monitor the use of, the My LUMs App. LUMS makes no representations relating to member use of the My LUMS or concerning the accuracy, completeness, or timeliness of any information found on the My LUMS App. LUMS is not responsible for screening communications and will not actively monitor the use of the My LUMS App. For this reason, it is essential that the Alumni users of the My LUMS App report any abuse or misuse of the My LUMS App by emailing us immediately. Participation in the My LUMS App is a privilege. My LUMS App reserve the right to suspend or terminate the accounts of any individuals who misuse the information contained in the My LUMS App or otherwise violate this user agreement.</p>
<p><strong><u>ELIGIBILITY</u></strong><br />
  If you graduated with a degree from SDSB LUMS, you are automatically a member of SDSB LUMS, along with nearly 3000 + &nbsp;of your fellow alumni. All alumni are eligible to register for My LUMS membership. Use of the My LUMS App indicates that you accept the Terms herein and agree to abide by them.</p>
<p><strong><u>PRIVACY</u></strong><br />
  Your privacy is a top priority to the Lahore University of Management Sciences (LUMS). <strong>Lums does not share, sell or trade alumni mailing lists (including alumni email addresses)</strong> with outside corporations or organizations, except for those that have business or contractual relationships with the University. Any third parties who receive alumni information are prohibited from using or sharing alumni information for any purpose other than offering or providing approved services to alumni.</p>
<p>LUMS takes reasonable precautions to assure overall system security by monitoring security issues and industry trends, to protect the privacy of My LUMS members and secure the personal information available through My LUMS. All areas of My LUMS that contain private information are housed on a secure server.</p>
<p>The My LUMS App is also password-protected to allow access only to registered LUMS alumni. Although these precautions should help to protect any personal information available through My LUMS from abuse or outside interference, a certain degree of privacy risk is faced any time information is shared over the Internet or through a downloaded app. Therefore, My LUMS members have the ability to selectively hide the personal information that is listed in the MY LUMS app to be viewed by other My LUMS members.</p>
<p>This is easily accomplished by clicking on the Privacy Preferences link on your My LUMS Profile page. LUMS invites you to explore this page, review your record, and select any and all privacy settings you wish to apply. </p>
<p>The information we do maintain about the user in My LUMS is available only under the following restricted circumstances:</p>
<ul type=\"disc\">
  <li>To LUMS Alumni who have registered with My LUMS (App).</li>
</ul>
<p>&nbsp;</p>
<p><strong><u>DATA POLICY</u></strong></p>
<p><strong>What kinds of information do we collect?</strong><br />
  To provide you with the best My LUMS App experience we must process information about you</p>
<ul>
  <li>Email address </li>
  <li>First name and last name </li>
  <li>Phone number </li>
  <li>Address, State, Province, ZIP/Postal code, City </li>
  <li>Cookies and Usage Data </li>
  <li>Usage Data </li>
  <li>We may also collect information that your browser sends whenever you visit our </li>
  <li>Service or when you access the Service by or through a mobile device (&quot;Usage Data&quot;) </li>
</ul>
<p>&nbsp;</p>
<p><strong><u>Things That You And Others Do And Provide</u></strong></p>
<ul type=\"disc\">
  <li><u>Information and content you provide. </u></li>
</ul>
<p>We collect the content, communications and other information you provide when you use our My LUMS App, including when you sign up for an account, create or share content and message or communicate with others. This can include information in or about the content that you provide (e.g. metadata), such as the location of a photo or the date a file was created. Our systems automatically process content and communications that you and others provide to analyze context and what's in them for the purposes described below. </p>
<ul type=\"disc\">
  <li><u>Networks and connections.</u></li>
</ul>
<p>We collect information about the people, accounts, you are connected to and how you interact with them across our Products, such as people you communicate with the most or groups that you are part of. </p>
<ul type=\"disc\">
  <li><u>Your usage. </u></li>
</ul>
<p>We collect information about how you use our App, such as the types of content that you view or engage with, the features you use, the actions you take, the people or accounts you interact with and the time, frequency and duration of your activities.We also collect information about how you use features such as our camera.</p>
<ul type=\"disc\">
  <li><u>Things others do and information that they provide about you.</u></li>
</ul>
<p>We also receive and analyze content, communications and information that other people provide when they use our My LUMS App. </p>
<p>SDSB LUMS Alumni App uses the collected data for various purposes:</p>
<ul>
  <li>To provide and maintain the Service. </li>
  <li>To notify you about changes to our service. </li>
  <li>To allow you to participate in interactive features of our Service when you choose to do so. </li>
  <li>To provide customer care and support. </li>
  <li>To provide analysis or valuable information so that we can improve the service. </li>
  <li>To monitor the usage of the Service. </li>
  <li>To detect, prevent and address technical issues. </li>
</ul>
<p>&nbsp;</p>
<p><strong><u>Device information</u></strong><br />
  As described below, we collect information collected through My LUMS App. Information that we obtain from these devices includes : </p>
<ul type=\"disc\">
  <li>Data from device settings: information that you allow us to receive      through device settings that you turn on, such as access to your GPS      location, camera or photos.</li>
  <li>Network and connections: information such as the mobile phone      number, information about other devices that are nearby or on your      network, so we can do things such as help you. </li>
  <li>Cookie data: data from cookies stored on your device, including      cookie IDs and settings. </li>
</ul>
<p><strong>&nbsp;</strong></p>
<p><strong><u>How do we use this information?</u></strong></p>
<p>We use the information that we have (subject to choices you make) as described below, and to provide and support LUMS App and related services described in the My LUMS terms and conditions. </p>
<p>Here's how:</p>
<p><strong><u>Provide, Personalize And Improve Our Services.</u></strong><br />
  We use the information we have to deliver our Products, and make suggestions for you (such as groups or events you may be interested in or topics you may want to follow) on and off our Products. To create personalized services that are unique and relevant to you, we use your connections, preferences, interests and activities based on the data that we collect and learn from you and others (including any data with special protections you choose to provide); how you use and interact with our Products; and the people, places or things that you're connected to and interested in on and off our services. </p>
<ul type=\"disc\">
  <li><u>Information Across My LUMS</u></li>
</ul>
<p>We connect information about your activities on My LUMS App to provide a more customized and consistent experience .We can also make your experience more seamless, for example, by automatically filling in your registration information (such as your phone number) as well as automatically syncing your LUMS profile with LinkedIn and Facebook.</p>
<p><u>&nbsp;</u></p>
<ul type=\"disc\">
  <li><u>Location-Related Information</u></li>
</ul>
<p>We use location related information - such as your current location, where you live, the places you like to go, and the businesses and people you're near - to provide, personalize and improve our Products, for you and others. Location-related information can be based on things such as precise device location (if you've allowed us to collect it), Information from your and others' use of My LUMS App (such as check-ins or events you attend).</p>
<ul type=\"disc\">
  <li><u>Product Research and Development</u></li>
</ul>
<p>We use the information we have to develop, test and improve our Products, including by conducting surveys and research, and testing and troubleshooting new products and features.</p>
<p><strong><u>TRANSFER OF DATA </u></strong><br />
  Your information, including Personal Data, may be transferred to - and maintained on - computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.<br />
  <br />
  If you are located outside Pakistan and choose to provide information to us, please note that we transfer the data, including Personal Data, to Pakistan and process it there. </p>
<p><strong><u>PROMOTE SAFETY, INTEGRITY AND SECURITY</u></strong><br />
  We use the information that we have to verify accounts and activity, combat harmful conduct, detect and prevent spam and other bad experiences, maintain the integrity of our Products, and promote safety and security on and off My LUMS services. For example, we use data that we have to investigate suspicious activity or breaches of our security. </p>
<p><strong><u>COMMUNICATE WITH YOU</u></strong><br />
  We use the information that we have to send you marketing communications, communicate with you about our services and let you know about our Policies and Terms. We also use your information to respond to you when you contact us.</p>
<p><strong><u>EMAIL</u></strong><br />
  By registering for the My LUMS, members consent to receive emails from Lahore University Of Management Sciences. My LUMS will send periodic news and information to My LUMS affiliates through e-mail. </p>
<p><strong><u>PASSWORD CONFIDENTIALITY</u></strong><br />
  Please do not give your \"My LUMS\" password to anyone. Together with your login name, your password is your key to managing your My LUMS information. Your login name and password provide easy access to your directory profile, Email Forwarding for Life, career tools, and other confidential services.</p>
<p><strong><u>LIMITATION OF LIABILITY</u></strong><br />
  In no event shall Lahore University Of Management Sciences, and its trustees, directors, officers, employees, and agents be liable for any direct, indirect, punitive, incidental, special, consequential, or other damages arising out of or in any way connected with the use of the My LUMS or with the delay or inability to use My LUMS, or for any information, software, products, and services obtained through the My LUMS, or otherwise arising out of the use of the My LUMS app. Whether based on contract, tort, strict liability, or otherwise. The user hereby specifically acknowledges and agrees that neither LUMS, nor their respective trustees, directors, officers, employees, or agents shall be liable for any defamatory, offensive, or illegal conduct of any user of the My LUMS (App).</p>
<p>Please note that Lahore University Of Management Sciences reserve the right to change this user agreement at any time. Your continued use of My LUMS (App) indicates your acceptance of such changes.</p>";
		$this->load->view('cms.php',$data);
	}
	
	public function pages($page_id = NULL)
	{
		$path = base_url('/uploads/');
		$profile = base_url('/events/gallery/');
		$this->db->select('*');
		if($page_id)
		{
			$this->db->or_where("id",$page_id);
			$this->db->or_where("slug",$page_id);
		}
		$ListTypes = $this->db->get('pages');
		
		if($page_id)
		{
			$ListTypes = $ListTypes->row_array();
		} else {
 			$ListTypes = $ListTypes->result_array();
		}
		return $ListTypes;
	}
	
}
