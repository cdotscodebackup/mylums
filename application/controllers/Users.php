<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require('Api.php');

class Users extends Api 
{
	public $content_type = "application/json";
	public $columns;
	public $columnsEdit;
	
	public function __construct()
    {
        parent::__construct();
		
		$columns = array();
		//$columns['email'] = 'Email';
		$columns['first_name'] = 'First Name';
		$columns['last_name'] = 'Last Name';
		$columns['gender'] = 'Gender';
		$columns['phone'] = 'Phone';
		$columns['department'] = 'Department';
		$columns['company'] = 'Company';
		$columns['designation'] = 'Designation';		
		$columns['roll_number'] = 'Roll Number';
		$columns['country'] = 'Country';
		$columns['city'] = 'City';
		//$columns['status'] = 'Status';
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'users___email', 				'database' => 'email',				'title' => 'Email', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'users___roll_number', 		'database' => 'roll_number',		'title' => 'Roll Number', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'profile___first_name', 		'database' => 'first_name',			'title' => 'First Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___last_name', 		'database' => 'last_name',			'title' => 'Last Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___gender', 			'database' => 'gender',				'title' => 'Gender', 'status' => 'required','type' => '');

	$columnsEdit[] = array('field' =>'profile___program',	 		'database' => 'program',			'title' => 'Program', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___year', 				'database' => 'year',				'title' => 'Year', 'status' => 'required','type' => '');	
	
	$columnsEdit[] = array('field' =>'profile___phone', 			'database' => 'phone',				'title' => 'Phone', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___landline', 			'database' => 'landline',			'title' => 'Landline', 'status' => '','type' => '');
	
	$columnsEdit[] = array('field' =>'profile___personal_email',	'database' => 'personal_email',		'title' => 'Personal Email', 'status' => '','type' => '');
	$columnsEdit[] = array('field' =>'profile___alternative_email',	'database' => 'alternative_email',	'title' => 'Alternative Email', 'status' => '','type' => '');
	
	
	$columnsEdit[] = array('field' =>'profile___current_company', 			'database' => 'current_company',			'title' => 'Current Company', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_industry', 			'database' => 'current_industry',			'title' => 'Current Industry', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_designation', 		'database' => 'current_designation',		'title' => 'Current Designation', 'status' => 'required', 'type' => '');	
	$columnsEdit[] = array('field' =>'profile___current_department', 		'database' => 'current_department',			'title' => 'Current Department', 'status' => 'required', 'type' => '');
	
	$columnsEdit[] = array('field' =>'profile___country', 			'database' => 'country',			'title' => 'Country', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___state_province',	'database' => 'state_province',		'title' => 'State/Province', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___city', 				'database' => 'city',				'title' => 'City', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___address',			'database' => 'address',			'title' => 'Address', 'status' => '','type' => '');
	
	//$columnsEdit[] = array('field' =>'users___status', 				'database' => 'status',				'title' => 'Status', 'status' => 'required', 'type' => '');
	$columnsEdit[] = array('field' =>'users___password', 			'database' => '',					'title' => 'Password','status' => 'required','type' => 'password');

		$this->columnsEdit = $columnsEdit;
	}
	
	public function profile($id = NULL)
	{
		if($id)
		{
			$userID = $id;
			$response = $this->user_model->get_user_object($userID);
			if($response)
			{
				$response['status'] = 'Successful';
				$response['message'] = 'User Profile';
				$this->api_model->response($this->json->encode($response),200);
			} else {
				$response = NULL;
				$response['status'] = 'Error';
				$response['message'] = 'User not found';
				$this->api_model->response($this->json->encode($response),404);
			}
		} else {
			$userID = $this->currentUserId->id;
			$response = $this->user_model->get_user_object($userID);
			if($response)
			{
				$headers = $this->input->request_headers();
				//$token = Authorization::validateToken($headers['Authorization']);
				//var_dump($token);
				$response['message'] = 'User Profile';
				$response['token'] = $headers['Authorization'];
				$response['status'] = 'Successful';
				$this->api_model->response($this->json->encode($response),200);
			} else {
				$response['status'] = 'Error';
				$response['message'] = 'User not found';
				$this->api_model->response($this->json->encode($response),404);
			}
		}
	}
	
	public function profile_qrcode($id = NULL)
	{
			header("Content-Type: image/png");
			$userID = $this->currentUserId->id;
			$userData = $this->user_model->profile_get($userID);
			
			$response['user']['id'] = $userData['user_id'];
			$response['user']['name'] = $userData['first_name'] . ' '.$userData['last_name'] ;
			$response['user']['email'] = $userData['email'];
			$response['user']['phone'] = $userData['phone'];
			
			$QRCode = base_url('qrcode/index.php/?data=') . urlencode( $this->json->encode($response) );
			echo file_get_contents( $QRCode );
			
			//$response['status'] = 'Successful';
			//$response['message'] = 'My Profile';
			//$this->api_model->response($this->json->encode($response),200);
	}
	
	public function profile_update()
	{
		
		$postKeyArray = array_keys($this->input->post());
		//var_dump($postKeyArray);
		
		$userID = $this->currentUserId->id;
		
		if(count($this->columnsEdit) > 0) 
		{ 
			$feildCounter = 0;
			foreach($this->columnsEdit as $columnRow)
			{
				if(in_array($columnRow['field'], $postKeyArray))
				{
					//echo $columnRow['field'], $columnRow['title'], $columnRow['status'];
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
		}
		//var_dump($this->columnsEdit);
		//var_dump($_POST);
		
		if($this->input->post('interests'))
		{
			$this->addinterests('update');
		}
		
		if($this->input->post('work_experience'))
		{
			$this->addworkexperience('update');
		}
		
		if ($this->form_validation->run())
		{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = '*';
			$config['encrypt_name'] = TRUE;
			//$config['max_size']	= '100';
			//$config['max_width']  = '1024';
			//$config['max_height']  = '768';
			
			$this->load->library('upload', $config);
			
			$data['upload_data']['file_name'] = NULL;
			if ( ! $this->upload->do_upload('image'))
			{
				$data['error'] = $this->upload->display_errors();
			} else {
				$data['upload_data'] = $this->upload->data();
			}
			
			if($data['upload_data']['file_name'])
			{
				$data_to_store['profile.image'] = $data['upload_data']['file_name'];
			}
			
			//var_dump($data);
			
			if(count($this->columnsEdit) > 0) 
			{ 
				foreach($this->columnsEdit as $column)
				{
					if(in_array($column['field'], $postKeyArray))
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
			}
			
			$country_id = $this->input->post('profile___country_id');
			if($country_id)
			{
				$data_to_store['profile.country_id'] = $country_id;
			}
			
			$state_id = $this->input->post('profile___state_id');
			if($state_id)
			{
				$data_to_store['profile.state_id'] = $state_id;
			}
			
			$city_id = $this->input->post('profile___city_id');
			if($city_id)
			{
				$data_to_store['profile.city_id'] = $city_id;
			}
			
			$data_to_store['users.updated_at'] = time();
			
			
			$this->user_model->profile_update($userID,$data_to_store);
			
			$interests = $this->user_model->interests($userID);
			$workexperience = $this->user_model->workexperience($userID);
			
			$response['user'] =  $this->user_model->profile_get($userID);
			
			$response['user']['interests'] = $interests;
			$response['user']['workexperience'] = $workexperience;
			
			$response['user']['image'] =  base_url('/uploads/') .$response['user']['image'];
			$headers = $this->input->request_headers();
			$response['token'] = $headers['Authorization'];
			$response['status'] = 'Successful';
			$response['message'] = 'Profile updated successfully.';
			$this->api_model->response($this->json->encode($response),200);
			return;
			
		} else {
			//$this->load->view('errors');
			$response['status'] = 'Error';
			$response['message'] = 'Fields is required.';
			$this->api_model->response($this->json->encode($response),406);
			return;
		}
		
		
		
		return false;
		$userID = $this->currentUserId->id;
		$response['user'] =  $this->user_model->profile_get($userID);
		$response['user']['image'] =  base_url('/uploads/') .$response['user']['image'];
		$response['status'] = 'Successful';
		$response['message'] = 'Profile successfully';
		$this->api_model->response($this->json->encode($response),200);
		
		
	}
	
	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			
			$data['data'] = array();
			$data['status'] = 'Not Found';
			$this->api_model->response($this->json($data),404);
		}
	}
	
	public function logout()
	{
		$userID = $this->currentUserId->id;
		if($this->input->get('user_id'))
		{
			$userID = $this->input->get('user_id');
		}
		
		$this->user_model->logout($userID);
		$data = array();
		$data['message'] = 'Logout successfully';
		$data['status'] = 'Successful';
		$this->api_model->response($this->json->encode($data),200);
	}
	
	public function addinterests($status = NULL)
	{
		$user_id = $this->currentUserId->id;
		
		$interests = $interests_post = $this->input->post('interests');
		$interests = $this->json->decode($interests);
		
		if($interests_post)
		{
			$this->db->where('user_id', $user_id);
			$this->db->delete('user_interests');
		
			if(count($interests) > 0)
			{
				foreach($interests as $interest)
				{
					$data = array( 'user_id' => $user_id, 'interest_id' => $interest, 'created_at' => time() ); 				
					$this->db->insert('user_interests', $data);
				}
			}
		}
		
		if($status != 'update')
		{
			$response['interests'] = $this->user_model->interests($user_id);
			$response['status'] = 'Successful';
			$response['message'] = 'Profile interests updated successfully.';
			$this->api_model->response($this->json->encode($response),200);
		}
	}
	
	public function addworkexperience($status = NULL)
	{
		$user_id = $this->currentUserId->id;
		
		$work_experience = $work_experience_post = $this->input->post('work_experience');
		$work_experience = $this->json->decode($work_experience);
		//var_dump($work_experience);
		if($work_experience_post)
		{
			
			$this->db->where('user_id',$user_id);
			$this->db->delete('user_workexperience');
			
			if(count($work_experience) > 0)
			{
				foreach($work_experience as $workexperience)
				{
					$data = array();
					$data['user_id'] = $user_id;
					$data['company_name'] = $workexperience['company_name'];
					$data['industry_id'] = $workexperience['industry'];
					$data['designation'] = $workexperience['desingation'];
					$data['from_date'] = $workexperience['from_date'];
					$data['to_date'] = $workexperience['to_date'];
					$data['is_current_job'] = $workexperience['is_current_job'];
					$data['city'] = $workexperience['city'];
					$data['state'] = $workexperience['state'];
					$data['country'] = $workexperience['country'];
					$data['created_at'] = time();
					$this->db->insert('user_workexperience', $data);
				}
			}
		}
		
		if($status != 'update')
		{
			$response['workexperience'] = $this->user_model->workexperience($user_id);
			$response['status'] = 'Successful';
			$response['message'] = 'Profile interests updated successfully.';
			$this->api_model->response($this->json->encode($response),200);
		}
	}
	
	public function addlinkedin($status = NULL)
	{
		$user_id = $this->currentUserId->id;
		
		$linkedindata = $this->input->post('linkedindata');
		
		//var_dump($linkedindata);
		//die();
		
		$this->db->where('user_id',$user_id);
		$ListTypes = $this->db->get('linkedin');
		$ListTypes = $ListTypes->row_array();
		if($ListTypes)
		{
			$data['user_id'] = $user_id;
			$data['userinfo'] = $linkedindata; //arrayEncode($linkedindata);
			$this->db->update('linkedin', $data);
		} else {
			$data['user_id'] = $user_id;
			$data['userinfo'] = $linkedindata; //arrayEncode($linkedindata);
			$data['created_at'] = time();
			$this->db->insert('linkedin', $data);
		}
		
		$response['linkedin'] = $this->user_model->linkedin($user_id);
		$response['status'] = 'Successful';
		$response['message'] = 'Profile interests updated successfully.';
		$this->api_model->response($this->json->encode($response),200);
			
	}
	
	public function invite()
	{
		
	}
}
?>