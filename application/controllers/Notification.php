<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Messages Api

require('Api.php');

class Notification extends Api 
{
	public $content_type = "application/json";
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('notifications_model');
	}
	
	public function all()
	{
		//var_dump($this->currentUserId->id);
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$messages = base_url('/messages/messageboard/');
				//$this->db->select('*, CONCAT(\''.$path. '\', profile.image) as imagepath, CONCAT(\''.$profile. '\', profile.user_id) as profilepath');
				$this->db->select('*,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at');
				
				//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
				$this->db->where('receiver_id',$this->currentUserId->id);
				
				//$this->db->where('users.status', "active");
				//profile table name
				$this->db->order_by('updated_at','DESC');
				$this->db->from('notification');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				//var_dump($data['data']);
				$NewData = array();
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						if($row['type'] == 'event')
						{
							$event = $this->get_event($row['type_id']);
							$path = base_url('/events/detail/');
							$row['title'] = $event['name']; 
							$row['url'] = $path . $row['type_id'];
						}
						
						if($row['type'] == 'message')
						{
							$messages = base_url('/messages/messageboard/');
							$last_message = $this->get_last_message($row['type_id']);
							$row['message'] = $last_message['message']; 
							$row['url'] = $messages . $row['type_id'];
						}
						
						if($row['type'] == 'tinder')
						{
							$messages = base_url('/messages/messageboard/');
							$last_message = $this->get_last_message($row['type_id']);
							$row['message'] = $last_message['message'];
							$row['url'] = $messages . $row['type_id'];
						}
						
						$readpath = base_url('/notification/read/');
						$row['read'] = $readpath . $row['id'];
						
						if($row['sender_id'] != $this->currentUserId->id)
						{
							$row['profile'] =  $this->user_model->profile_get($row['sender_id']);
							$row['profile']['image'] =  base_url('/uploads/') . $row['profile']['image'];
						}
					}
				}
				
				//$data['data'] = $NewData;
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Notification list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Notification not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	function read()
	{
		$id = $this->uri->segment('3');
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->from('notification');
		$query = $this->db->get();
		$result = $query->row_array();
		if($result)
		{
			if($result['status'] != 'read')
			{
				$data = array('updated_at' => time(), 'status'  => 'read');
				$this->db->where('id', $id);
				$this->db->update('notification', $data);
				
				$data['status'] = 'Successful';
				$data['message'] = 'read';
				$this->api_model->response($this->json->encode($data),200);
			} else {
				$data['status'] = 'Successful';
				$data['message'] = 'Alread.';
				$this->api_model->response($this->json->encode($data),200);
			}
		} else {
			$data['status'] = 'Error';
			$data['message'] = 'Notification not found.';
			$this->api_model->response($this->json->encode($data),404);
		}
	}
	
	
	function get_event($event_id)
	{
		$arraylist = array();
		$this->db->where('id',$event_id);
		$ListTypes = $this->db->get('events');
		$ListTypes = $ListTypes->row_array();
		return $ListTypes;
	}
	
	function get_last_message($chatroom_id)
	{
		//SELECT message from chat_messages where chatroom_id = chats.id order by -id limit 1
		$arraylist = array();
		$this->db->where('chatroom_id',$chatroom_id);
		$this->db->order_by('id','-');
		$this->db->limit(1);
		$ListTypes = $this->db->get('chat_messages');
		$ListTypes = $ListTypes->row_array();
		return $ListTypes;
	}
}
?>