<?php
class Alumni extends CI_Controller 
{

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/alumni';
 	public $columns;
	public $columnsEdit;
	
	/**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('alumni_model');
		if(!$this->session->userdata('is_logged_in'))
		{
            redirect('admin/login');
        }
		
		$columns = array();
		$columns['email'] = 'Email';
		$columns['first_name'] = 'First Name';
		$columns['last_name'] = 'Last Name';
		$columns['gender'] = 'Gender';
		$columns['phone'] = 'Phone';
		$columns['current_industry'] = 'Industry';
		$columns['current_company'] = 'Company';
		$columns['current_designation'] = 'Designation';		
		$columns['roll_number'] = 'Roll Number';
		$columns['country'] = 'Country';
		$columns['city'] = 'City';
		$columns['status'] = 'Status';
		//$columns['password'] = 'Password';
		
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'users___email', 				'database' => 'email',				'title' => 'Email', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'users___roll_number', 		'database' => 'roll_number',		'title' => 'Roll Number', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'profile___first_name', 		'database' => 'first_name',			'title' => 'First Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___last_name', 		'database' => 'last_name',			'title' => 'Last Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___gender', 			'database' => 'gender',				'title' => 'Gender', 'status' => 'required','type' => '');

	$columnsEdit[] = array('field' =>'profile___program',	 		'database' => 'program',			'title' => 'Program', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___year', 				'database' => 'year',				'title' => 'Year', 'status' => 'required','type' => '');	
	
	$columnsEdit[] = array('field' =>'profile___phone', 			'database' => 'phone',				'title' => 'Phone', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___landline', 			'database' => 'landline',			'title' => 'Landline', 'status' => 'required','type' => '');
	
	$columnsEdit[] = array('field' =>'profile___personal_email',	'database' => 'personal_email',		'title' => 'Personal Email', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___alternative_email',	'database' => 'alternative_email',	'title' => 'Alternative Email', 'status' => 'required','type' => '');
	
	
	$columnsEdit[] = array('field' =>'profile___current_company', 			'database' => 'current_company',			'title' => 'Current Company', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_industry', 			'database' => 'current_industry',			'title' => 'Current Industry', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_designation', 		'database' => 'current_designation',		'title' => 'Current Designation', 'status' => 'required', 'type' => '');	
	$columnsEdit[] = array('field' =>'profile___current_department', 		'database' => 'current_department',			'title' => 'Current Department', 'status' => 'required', 'type' => '');
	
	$columnsEdit[] = array('field' =>'profile___country', 			'database' => 'country',			'title' => 'Country', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___state_province',	'database' => 'state_province',		'title' => 'State/Province', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___city', 				'database' => 'city',				'title' => 'City', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___address',			'database' => 'address',			'title' => 'Address', 'status' => '','type' => '');
	
	$columnsEdit[] = array('field' =>'users___status', 				'database' => 'status',				'title' => 'Status', 'status' => 'required', 'type' => '');
	$columnsEdit[] = array('field' =>'users___password', 			'database' => '',					'title' => 'Password','status' => '','type' => 'password');

		$this->columnsEdit = $columnsEdit;
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
		$searchField = $this->input->post('searchField'); 
        $order_type = $this->input->post('order_type');
		$category = $this->input->post('category');
		$CatMain = $this->input->post('CatMain');
		

        //pagination settings
        $config['per_page'] = 10;

        $config['base_url'] = base_url().'admin/alumni/';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
		//
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;
		


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false && $searchField !== false || $this->uri->segment(3) == true)
		{ 
          
		  if($this->input->get('rs') == 'yes')
			{
				$filter_session_data['manufacture_selected'] = null;
				$filter_session_data['search_string_selected'] = null;
				$filter_session_data['order'] = null;
				$filter_session_data['searchField'] = null;
				$filter_session_data['order_type'] = null;
				$filter_session_data['category'] = null;
				$filter_session_data['category'] = null;
				$this->session->set_userdata($filter_session_data);
			}
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string)
			{
                $filter_session_data['search_string_selected'] = $search_string;
            } else {
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order)
			{
                $filter_session_data['order'] = $order;
            } else {
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;
			
			if($searchField)
			{
                $filter_session_data['searchField'] = $searchField;
            } else {
                $searchField = $this->session->userdata('searchField');
            }
            $data['searchField'] = $searchField;
			
			///
			if($category)
			{
                $filter_session_data['category'] = $category;
            } else {
                $category = $this->session->userdata('category');
            }
            $data['category'] = $category;
			
			if($CatMain) 
			{
                $filter_session_data['CatMain'] = $CatMain;
            } else {
                $CatMain = $this->session->userdata('CatMain');
            }
            $data['CatMain'] = $CatMain;
			
			///
			

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);
            } 
            
            //fetch sql data into arrays
            $data['count_products']= $this->alumni_model->count_manufacturers($search_string, $order);
            $config['total_rows'] = $data['count_products'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['manufacturers'] = $this->alumni_model->get_manufacturers($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->alumni_model->get_manufacturers($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['manufacturers'] = $this->alumni_model->get_manufacturers('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->alumni_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
			$filter_session_data['searchField'] = null;
            $filter_session_data['order_type'] = null;
			$filter_session_data['category'] = null;
			$filter_session_data['category'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';
			$data['searchField'] = '';
			$data['category'] = '';
			$data['CatMain'] = '';
			

            //fetch sql data into arrays
            $data['count_products']= $this->alumni_model->count_manufacturers();
            $data['manufacturers'] = $this->alumni_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_products'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
		$directory = $this->uri->segment(2);
		$data['columns'] = $this->columns;
        $data['main_content'] = 'admin/'.$directory.'/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				
				
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				$download['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('download'))
				{
					$download['error'] = $this->upload->display_errors();
				} else {
					$download['upload_data'] = $this->upload->data();
					
					$this->load->library('image_lib');
					
				}

                $data_to_store['P_Name'] = $this->input->post('name');
				//$data_to_store['P_Type'] = $this->input->post('type');
				$data_to_store['P_Description'] = $this->input->post('description');
				$data_to_store['CatID'] = $this->input->post('category');
				$data_to_store['P_Featured'] = $this->input->post('featured');
				if($data['upload_data']['file_name'])
				{
					$data_to_store['P_Path'] = $data['upload_data']['file_name'];
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['P_Downloadable'] = $download['upload_data']['file_name'];
				}
				$data_to_store['datentime'] = time();
				
                //if the insert has returned true then we show the flash message
                if($this->alumni_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		$data['allcategories'] = $this->categories_model->get_manufacturers();
		$directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/add';
        $this->load->view('includes/template', $data);
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            //$this->form_validation->set_rules('name', 'Name', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    			
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['profile.image'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				$data_to_store['users.updated_at'] = time();
				$data_to_store['profile.updated_at'] = time();
				//var_dump($data_to_store);
				//die();
                //if the insert has returned true then we show the flash message
				
                if($this->alumni_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
				$directory = $this->uri->segment(2);
                redirect('admin/'.$directory.'/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
        $data['manufacture'] = $this->alumni_model->get_manufacture_by_id($id);
        $directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/edit';
        $this->load->view('includes/template', $data);            

    }//update

	
	public function uploadfile()
    {
		$data['file'] = '';
		$this->load->library('csvreader');
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            if(!$_FILES)
			{
				$this->form_validation->set_rules('image', 'File', 'required');
			}
			$this->form_validation->set_rules('fileuploading', 'fileuploading', 'required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				$result =   $this->csvreader->parse_file('./uploads/' . $data['upload_data']['file_name']);
				$counter = array();
				if(count($result) > 0)
				{
					foreach($result as $row)
					{
						$counter[] = $this->alumni_model->verify_insert($row);
					}
				}
				
				$data['file'] = count($counter);
				//$result =   $this->csvreader->parse_file('./uploads/dabf591fc386c74c1c46a36eb041e7b3.csv');
				//$data['file'] = $result;
				//var_dump( $result );
				//die();
            }
        }
		
		//$result = $this->csvreader->parse_file('./uploads/dabf591fc386c74c1c46a36eb041e7b3.csv');
		/*$counter = array();
		if(count($result) > 0)
		{
			foreach($result as $row)
			{
				$counter[] = $this->alumni_model->verify_insert($row);
			}
		}
		
		$data['file'] = $counter;*/
		
		//load the view
		//$data['allcategories'] = $this->categories_model->get_manufacturers();
		$directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/upload';
        $this->load->view('includes/template', $data);
    }
	
	public function preload()
	{
		$event_id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		/*$users = $this->input->post('users');
		if($users)
		{
			$this->add_users($users,$event_id);
			$this->session->set_flashdata('flash_message', 'updated');
		}*/
		
		//$data['manufacture'] = $this->events_model->get_manufacture_by_id($event_id);
		//$data['images'] = $this->events_model->get_gallery($id);
        $directory = $this->uri->segment(2);
		$data['controller']=$this; 
        $data['users'] = $this->db->get('preload')->result_array();
		$data['main_content'] = 'admin/'.$directory.'/preload';
        $this->load->view('includes/template', $data);
	}
	
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
        $this->alumni_model->delete_manufacture($id);
        redirect('admin/' . $url);
    }//edit

}