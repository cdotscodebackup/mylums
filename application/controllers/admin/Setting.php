<?php
class Setting extends CI_Controller 
{

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/setting';
 	public $columns;
	public $columnsEdit;
	
	/**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('setting_model','setting');
		if(!$this->session->userdata('is_logged_in'))
		{
            redirect('admin/login');
        }
		
		$columns = array();
		$columns['email'] = 'Email';
		$columns['first_name'] = 'First Name';
		$columns['last_name'] = 'Last Name';
		$columns['gender'] = 'Gender';
		$columns['phone'] = 'Phone';
		$columns['current_industry'] = 'Industry';
		$columns['current_company'] = 'Company';
		$columns['current_designation'] = 'Designation';		
		$columns['roll_number'] = 'Roll Number';
		$columns['country'] = 'Country';
		$columns['city'] = 'City';
		$columns['status'] = 'Status';
		//$columns['password'] = 'Password';
		
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'users___email', 				'database' => 'email',				'title' => 'Email', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'users___roll_number', 		'database' => 'roll_number',		'title' => 'Roll Number', 'status' => '','type' => 'disable');
	$columnsEdit[] = array('field' =>'profile___first_name', 		'database' => 'first_name',			'title' => 'First Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___last_name', 		'database' => 'last_name',			'title' => 'Last Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___gender', 			'database' => 'gender',				'title' => 'Gender', 'status' => 'required','type' => '');

	$columnsEdit[] = array('field' =>'profile___program',	 		'database' => 'program',			'title' => 'Program', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___year', 				'database' => 'year',				'title' => 'Year', 'status' => 'required','type' => '');	
	
	$columnsEdit[] = array('field' =>'profile___phone', 			'database' => 'phone',				'title' => 'Phone', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___landline', 			'database' => 'landline',			'title' => 'Landline', 'status' => 'required','type' => '');
	
	$columnsEdit[] = array('field' =>'profile___personal_email',	'database' => 'personal_email',		'title' => 'Personal Email', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___alternative_email',	'database' => 'alternative_email',	'title' => 'Alternative Email', 'status' => 'required','type' => '');
	
	
	$columnsEdit[] = array('field' =>'profile___current_company', 			'database' => 'current_company',			'title' => 'Current Company', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_industry', 			'database' => 'current_industry',			'title' => 'Current Industry', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___current_designation', 		'database' => 'current_designation',		'title' => 'Current Designation', 'status' => 'required', 'type' => '');	
	$columnsEdit[] = array('field' =>'profile___current_department', 		'database' => 'current_department',			'title' => 'Current Department', 'status' => 'required', 'type' => '');
	
	$columnsEdit[] = array('field' =>'profile___country', 			'database' => 'country',			'title' => 'Country', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___state_province',	'database' => 'state_province',		'title' => 'State/Province', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___city', 				'database' => 'city',				'title' => 'City', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'profile___address',			'database' => 'address',			'title' => 'Address', 'status' => '','type' => '');
	
	$columnsEdit[] = array('field' =>'users___status', 				'database' => 'status',				'title' => 'Status', 'status' => 'required', 'type' => '');
	$columnsEdit[] = array('field' =>'users___password', 			'database' => '',					'title' => 'Password','status' => '','type' => 'password');

		$this->columnsEdit = $columnsEdit;
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$event_id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            //$this->form_validation->set_rules('name', 'Name', 'required');
			
			$this->form_validation->set_rules('eventreminder', 'Event Reminder', 'required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    			
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('homescreen'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					//$data_to_store['photo'] = $data['upload_data']['file_name'];
					$this->setting->update('homescreen',$data['upload_data']['file_name']);
				}
				
				$this->setting->update('eventreminder',$this->input->post('eventreminder'));
				
				//$data_to_store['profile.updated_at'] = time();
				//var_dump($data_to_store);
				//die();
                //if the insert has returned true then we show the flash message
				
                /*if($this->events_model->update_manufacture($id, $data_to_store) == TRUE)
				{
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }*/
				
				$this->session->set_flashdata('flash_message', 'updated');
				/*$directory = $this->uri->segment(2);
                redirect('admin/'.$directory.'/update/'.$id.'');*/

            }//validation run

        }
		
		$data['eventreminder'] = $this->setting->get('eventreminder');
		$data['homescreen'] = $this->setting->get('homescreen');
        $directory = $this->uri->segment(2);
		$data['main_content'] = 'admin/'.$directory.'/index';
        $this->load->view('includes/template', $data);
	}
	//index
	
	function updateseeting()
	{
		
	}
}