<?php
class Industry extends CI_Controller 
{

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/industry';
 	public $columns;
	public $columnsEdit;
	
	/**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('industry_model');
		if(!$this->session->userdata('is_logged_in'))
		{
            redirect('admin/login');
        }
		
		$columns = array();
		$columns['name'] = 'Name';
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'name','database' => 'name','title' => 'Name', 'status' => 'required','type' => '');
	$this->columnsEdit = $columnsEdit;
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
		$searchField = $this->input->post('searchField'); 
        $order_type = $this->input->post('order_type');
		$category = $this->input->post('category');
		$CatMain = $this->input->post('CatMain');
		

        //pagination settings
        $config['per_page'] = 10;

        $config['base_url'] = base_url().'admin/industry/';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
		//
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;
		


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false && $searchField !== false || $this->uri->segment(3) == true)
		{
			if($this->input->get('rs') == 'yes')
			{
				$filter_session_data['manufacture_selected'] = null;
				$filter_session_data['search_string_selected'] = null;
				$filter_session_data['order'] = null;
				$filter_session_data['searchField'] = null;
				$filter_session_data['order_type'] = null;
				$filter_session_data['category'] = null;
				$filter_session_data['category'] = null;
				$this->session->set_userdata($filter_session_data);
			}
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string)
			{
                $filter_session_data['search_string_selected'] = $search_string;
            } else {
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order)
			{
                $filter_session_data['order'] = $order;
            } else {
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;
			
			if($searchField)
			{
                $filter_session_data['searchField'] = $searchField;
            } else {
                $searchField = $this->session->userdata('searchField');
            }
            $data['searchField'] = $searchField;
			
			///
			if($category)
			{
                $filter_session_data['category'] = $category;
            } else {
                $category = $this->session->userdata('category');
            }
            $data['category'] = $category;
			
			if($CatMain) 
			{
                $filter_session_data['CatMain'] = $CatMain;
            } else {
                $CatMain = $this->session->userdata('CatMain');
            }
            $data['CatMain'] = $CatMain;
			
			///
			

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);
            }
            
            //fetch sql data into arrays
            $data['count_products']= $this->industry_model->count_manufacturers($search_string, $order);
            $config['total_rows'] = $data['count_products'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['manufacturers'] = $this->industry_model->get_manufacturers($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->industry_model->get_manufacturers($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['manufacturers'] = $this->industry_model->get_manufacturers('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->industry_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
			$filter_session_data['searchField'] = null;
            $filter_session_data['order_type'] = null;
			$filter_session_data['category'] = null;
			$filter_session_data['category'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';
			$data['searchField'] = '';
			$data['category'] = '';
			$data['CatMain'] = '';
			

            //fetch sql data into arrays
            $data['count_products']= $this->industry_model->count_manufacturers();
            $data['manufacturers'] = $this->industry_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_products'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
		$directory = $this->uri->segment(2);
		$data['columns'] = $this->columns;
        $data['main_content'] = 'admin/'.$directory.'/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
           if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				
				
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				$download['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('download'))
				{
					$download['error'] = $this->upload->display_errors();
				} else {
					$download['upload_data'] = $this->upload->data();
					
					$this->load->library('image_lib');
					
				}

              
				if($data['upload_data']['file_name'])
				{
					$data_to_store['P_Path'] = $data['upload_data']['file_name'];
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['P_Downloadable'] = $download['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				$data_to_store['created_at'] = time();
				
                //if the insert has returned true then we show the flash message
                if($this->industry_model->store_manufacture($data_to_store))
				{
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		//$data['allcategories'] = $this->categories_model->get_manufacturers();
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
		
		$directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/add';
        $this->load->view('includes/template', $data);
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            //$this->form_validation->set_rules('name', 'Name', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    			
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['profile.image'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				$data_to_store['updated_at'] = time();
				//$data_to_store['profile.updated_at'] = time();
				//var_dump($data_to_store);
				//die();
                //if the insert has returned true then we show the flash message
				
                if($this->industry_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
				$directory = $this->uri->segment(2);
                redirect('admin/'.$directory.'/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
        $data['manufacture'] = $this->industry_model->get_manufacture_by_id($id);
        $directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
        $this->industry_model->delete_manufacture($id);
        redirect('admin/' . $url);
		//edit
    }
}