<?php
class Events extends CI_Controller 
{

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/events';
 	public $columns;
	public $columnsEdit;
	
	/**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('events_model');
		if(!$this->session->userdata('is_logged_in'))
		{
            redirect('admin/login');
        }
		
		$columns = array();
		$columns['name'] = 'Name';
		//$columns['description'] = 'Description';
		$columns['dates'] = 'Date';
		$columns['start_time'] = 'Start Time';
		$columns['end_time'] = 'End Time';
		//$columns['members'] = 'Members';
		$columns['status'] = 'Status';
		
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'name','database' => 'name','title' => 'Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'description','database' => 'description','title' => 'Description', 'status' => '','type' => 'textarea');
	$columnsEdit[] = array('field' =>'dates','database' => 'dates','title' => 'Date', 'status' => 'required','type' => 'date');
	$columnsEdit[] = array('field' =>'start_time','database' => 'start_time','title' => 'Start time', 'status' => 'required','type' => 'time');
	$columnsEdit[] = array('field' =>'end_time','database' => 'end_time','title' => 'End time', 'status' => 'required','type' => 'time');
	$columnsEdit[] = array('field' =>'venue','database' => 'venue','title' => 'Venue', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'geo_location','database' => 'geo_location','title' => 'Geo Location', 'status' => 'required','type' => 'location');
	$columnsEdit[] = array('field' =>'status','database' => 'status','title' => 'Status', 'status' => 'required','type' => '');
	
	$this->columnsEdit = $columnsEdit;
    }
	
	
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
		$searchField = $this->input->post('searchField'); 
        $order_type = $this->input->post('order_type');
		$category = $this->input->post('category');
		$CatMain = $this->input->post('CatMain');
		

        //pagination settings
        $config['per_page'] = 10;

        $config['base_url'] = base_url().'admin/industry/';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
		//
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;
		


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false && $searchField !== false || $this->uri->segment(3) == true)
		{
			
           	if($this->input->get('rs') == 'yes')
			{
				$filter_session_data['manufacture_selected'] = null;
				$filter_session_data['search_string_selected'] = null;
				$filter_session_data['order'] = null;
				$filter_session_data['searchField'] = null;
				$filter_session_data['order_type'] = null;
				$filter_session_data['category'] = null;
				$filter_session_data['category'] = null;
				$this->session->set_userdata($filter_session_data);
			}
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string)
			{
                $filter_session_data['search_string_selected'] = $search_string;
            } else {
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order)
			{
                $filter_session_data['order'] = $order;
            } else {
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;
			
			if($searchField)
			{
                $filter_session_data['searchField'] = $searchField;
            } else {
                $searchField = $this->session->userdata('searchField');
            }
            $data['searchField'] = $searchField;
			
			///
			if($category)
			{
                $filter_session_data['category'] = $category;
            } else {
                $category = $this->session->userdata('category');
            }
            $data['category'] = $category;
			
			if($CatMain) 
			{
                $filter_session_data['CatMain'] = $CatMain;
            } else {
                $CatMain = $this->session->userdata('CatMain');
            }
            $data['CatMain'] = $CatMain;
			
			///
			

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);
            }
            
            //fetch sql data into arrays
            $data['count_products']= $this->events_model->count_manufacturers($search_string, $order);
            $config['total_rows'] = $data['count_products'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['manufacturers'] = $this->events_model->get_manufacturers($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->events_model->get_manufacturers($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['manufacturers'] = $this->events_model->get_manufacturers('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['manufacturers'] = $this->events_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
			$filter_session_data['searchField'] = null;
            $filter_session_data['order_type'] = null;
			$filter_session_data['category'] = null;
			$filter_session_data['category'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';
			$data['searchField'] = '';
			$data['category'] = '';
			$data['CatMain'] = '';
			

            //fetch sql data into arrays
            $data['count_products']= $this->events_model->count_manufacturers();
            $data['manufacturers'] = $this->events_model->get_manufacturers('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_products'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
		$directory = $this->uri->segment(2);
		$data['columns'] = $this->columns;
        $data['main_content'] = 'admin/'.$directory.'/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
           if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				
				
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['photo'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				if($this->input->post('geo_locationLat'))
				{
					$data_to_store['lat'] = $this->input->post('geo_locationLat');
				}
				
				if($this->input->post('geo_locationLon'))
				{
					$data_to_store['lon'] = $this->input->post('geo_locationLon');
				}
				
				$data_to_store['full_time'] = strtotime($this->input->post('dates') . ' ' . $this->input->post('start_time'));
				
				$data_to_store['created_at'] = time();
				
                //if the insert has returned true then we show the flash message
                if($this->events_model->store_manufacture($data_to_store))
				{
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		//$data['allcategories'] = $this->categories_model->get_manufacturers();
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
		
		$directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/add';
        $this->load->view('includes/template', $data);
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            //$this->form_validation->set_rules('name', 'Name', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    			
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['photo'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				if($this->input->post('geo_locationLat'))
				{
					$data_to_store['lat'] = $this->input->post('geo_locationLat');
				}
				
				if($this->input->post('geo_locationLon'))
				{
					$data_to_store['lon'] = $this->input->post('geo_locationLon');
				}
				$data_to_store['full_time'] = strtotime($this->input->post('dates') . ' ' . $this->input->post('start_time'));
				
				$data_to_store['updated_at'] = time();
				//$data_to_store['profile.updated_at'] = time();
				//var_dump($data_to_store);
				//die();
                //if the insert has returned true then we show the flash message
				
                if($this->events_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
				$directory = $this->uri->segment(2);
                redirect('admin/'.$directory.'/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
        $data['manufacture'] = $this->events_model->get_manufacture_by_id($id);
        $directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
        $this->events_model->delete_manufacture($id);
        redirect('admin/' . $url);
		//edit
    }
	
	public function gallery()
	{
		$id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		if($this->input->get('del'))
		{
			$this->events_model->remove_gallery($this->input->get('del'));
		}
		
        $data['manufacture'] = $this->events_model->get_manufacture_by_id($id);
		$data['images'] = $this->events_model->get_gallery($id);
        $directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/gallery';
        $this->load->view('includes/template', $data);    
	}
	
	function addgallery()
	{
		$event_id = $this->uri->segment(4);
		
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = TRUE;
		//$config['max_size']	= '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		
		$data['upload_data']['file_name'] = NULL;
		if ( ! $this->upload->do_upload('image'))
		{
		$data['error'] = $this->upload->display_errors();
		} else {
		$data['upload_data'] = $this->upload->data();
		}
		
		if($data['upload_data']['file_name'])
		{
			$data_to_store['event_id'] = $event_id;
			$data_to_store['type'] = 'photo';
			$data_to_store['path'] = $data['upload_data']['file_name'];
		}
		
		if($this->input->post('urls'))
		{
			$data_to_store['event_id'] = $event_id;
			$data_to_store['type'] = 'video';
			$data_to_store['path'] = $this->input->post('urls');
		}
		
		$this->events_model->add_gallery($data_to_store);
		?><script type="text/javascript">window.parent.pageLoad();</script><?php
	}
	
	public function setlocation()
	{
		$this->CurrentMethod 		= __FUNCTION__;
		$this->form_validation->set_rules('location_title', 'Location', 'trim');
		$this->form_validation->set_rules('location_lat', 'Location Latitude', 'trim|required');
		$this->form_validation->set_rules('location_lon', 'Location Longitude', 'trim|required');

		if ($this->form_validation->run() === true)
        {
			$this->response['status']	= "success";

			$user_location_full = $this->input->post('location_title'); 
			$user_location 		= explode(',', $this->input->post('location_title'));
			array_pop($user_location);
			$user_location 		= trim(end($user_location));
			$location_lat 		= $this->input->post('location_lat');
			$location_lon 		= $this->input->post('location_lon');

			$params 		= array(
								'sensor' 	=> 'false',
								'latlng'	=> $location_lat . ',' . $location_lon,
								'key'		=> 'AIzaSyBSozsDOYhjtqX8apR0AydJDDJZaI5ACO0' 
							);

			$content 		= file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'. http_build_query($params));
			$output 		= json_decode($content);

			if($output->status == 'OK')
			{
				$user_location = getLocality($output->results[0]->address_components)->long_name;
			}
			elseif($output->status == 'ZERO_RESULTS')
			{
				$this->response['status']	= "error";
				$this->response['message']	= "No Result Found.";
			}
			else
			{
				$this->response['status']	= "error";
				$this->response['message']	= "Something Went Wrong. Please Try Again.";
			}

			if($this->response['status'] != "error")
			{
				$this->response['message']	= "Location Updated Successfully." . http_build_query($params);
				$this->response['data']		= array(
													'user_location_full' => empty($user_location_full) ? $output->results[0]->formatted_address : $user_location_full,
													'user_location' => $user_location,
													'location_lat'	=> $this->input->post('location_lat'),
													'location_lon'	=> $this->input->post('location_lon'),
													''
												);
			}
		}
		else
		{
			$this->response['status']	= "error";
			$this->response['message']	= "Validation Erros.";
			$this->response['data']		= explode('|', validation_errors('', '|'));
		}
		die(json_encode($this->response));
	}
	
	public function invites()
	{
		$event_id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		$users = $this->input->post('users');
		if($users)
		{
			$this->add_users($users,$event_id);
			$this->session->set_flashdata('flash_message', 'updated');
		}
		
		$data['manufacture'] = $this->events_model->get_manufacture_by_id($event_id);
		//$data['images'] = $this->events_model->get_gallery($id);
        $directory = $this->uri->segment(2);
		$data['controller']=$this; 
        $data['users'] = $this->get_users();
		$data['main_content'] = 'admin/'.$directory.'/invites';
        $this->load->view('includes/template', $data);
	}
	
	function get_users()
	{
		$arraylist = array();
		$this->db->select('*');
		$this->db->where('user_type != \'admin\'');
		$this->db->where('status','active');
		$this->db->where('profile.user_id=users.id');
		$this->db->from('users,profile');
		$ListTypes = $this->db->get();
		//echo $this->db->last_query();
		$ListTypes = $ListTypes->result_array();
		return $ListTypes;
	}
	
	function add_users($data,$event_id)
	{
		//var_dump($data);
		if(count($data) > 0)
		{
			foreach($data as $row)
			{
				$this->insert_user($row,$event_id);
			}
		}
	}
	
	public function get_invited($user_id,$event_id)
	{
		$this->db->where('receiver_id',$user_id);
		$this->db->where('type_id',$event_id);
		$this->db->where('type','event');
		$this->db->from('notification');
		$ListTypes = $this->db->get();
		$ListTypes = $ListTypes->row_array();
		if($ListTypes)
		{
			return $ListTypes;
		}
	}
	
	function insert_user($user_id,$event_id)
	{
		$this->db->where('receiver_id',$user_id);
		$this->db->where('type_id',$event_id);
		$this->db->where('type','event');
		$this->db->from('notification');
		$ListTypes = $this->db->get();
		$ListTypes = $ListTypes->row_array();
		if($ListTypes)
		{
			return $ListTypes;
		} else {
			$sender_id = $this->session->userdata('user_id');
			$data = array();
			$data['receiver_id'] = $user_id;
			$data['sender_id'] = $sender_id;
			$data['type_id'] = $event_id;
			$data['created_at'] = time();
			$data['status'] = 'unread';
			$data['type'] = 'event';
			$insert = $this->db->insert('notification', $data);
			
			// push notification function;
	    	return $insert;
		}
	}
}
?>