<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Alumni Api

require('Api.php');

class Events extends Api 
{
	public $content_type = "application/json";
	public $columns;
	public $columnsEdit;
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
	}
	
	public function index()
	{
	
	}
	
	public function all()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/detail/');
				$this->db->select('*,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member,
								  CONCAT(\''.$path. '\', photo) as imagepath,
								  CONCAT(\''.$profile. '\', id) as eventpath');
			
				if($this->input->get('q'))
				{
					$this->db->or_like('name', $this->input->get('q') );
					$this->db->or_like('description', $this->input->get('q') );
				}
				
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				//var_dump($data['data']);
				
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						$row['date_time'] = date('d-m-Y h:i a', strtotime($row['dates'] . ' ' . $row['start_time']));	
					}
				}
				
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Events list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function past()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/detail/');
				$this->db->select('*, 
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as eventpath');
				if($this->input->get('q'))
				{
					$search = $this->input->get('q');
					$arrays = stringArray($search,' ');
					if(count($arrays) > 0)
					{
						$this->db->group_start();
						foreach($arrays as $arrayRow)
						{
							$this->db->group_start();
							$this->db->or_like('name',$arrayRow); 
							$this->db->or_like('description',$arrayRow);
							$this->db->group_end();
						}
						$this->db->group_end();
					}
				}
				
				//$this->db->where('status', "expire");
				$this->db->where('full_time <', time());
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->result_array();
				
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						$row['date_time'] = date('d-m-Y h:i a', strtotime($row['dates'] . ' ' . $row['start_time']));
						$row['status'] = 'expire';
					}
				}
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Events list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function upcoming()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/detail/');
				$this->db->select('*, 
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as eventpath');
				
				if($this->input->get('q'))
				{
					$search = $this->input->get('q');
					$arrays = stringArray($search,' ');
					if(count($arrays) > 0)
					{
						$this->db->group_start();
						foreach($arrays as $arrayRow)
						{
							$this->db->group_start();
							$this->db->or_like('name',$arrayRow); 
							$this->db->or_like('description',$arrayRow);
							$this->db->group_end();
						}
						$this->db->group_end();
					}
				}
				
				$this->db->where('status', "active");
				$this->db->where('full_time >=', time());
				$this->db->order_by("full_time", "ASC"); //DESC
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->result_array();
				
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						$row['date_time'] = date('d-m-Y h:i a', strtotime($row['dates'] . ' ' . $row['start_time']));	
					}
				}
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Events list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function detail()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/gallery/');
				$this->db->select('*, 
								  DATE_FORMAT(dates, "%d-%m-%Y") as dates,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as gallery');
				
				
				$event_id = $this->uri->segment(3);
				$this->db->where('id', $event_id);
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->row_array();
				if(count($data['data']) > 0)
				{
					$totalCheckedIn = $this->checkedintotal($event_id);
					$interests = $this->interests($event_id);
					$gallery = $this->media($event_id);
					$mystatus = $this->mystatus($event_id);
					$checkin = $this->mystatuscheckin($event_id);
					$data['data']['date_time'] = date('d-m-Y h:i a', strtotime($data['data']['dates'] . ' ' . $data['data']['start_time']));
					$data['data']['mystatus'] = $mystatus;
					$data['data']['mycheckin'] = $checkin;
					$data['data']['gallery'] = $gallery;
					$data['data']['interests'] = $interests;
					$data['data']['users'] = $this->userstotal($event_id);
					$data['data']['users']['checkedin'] = $totalCheckedIn['users'];
					$data['data']['users']['checkedin-total'] = $totalCheckedIn['total'];
					$data['status'] = 'Successful';
					$data['message'] = 'Events Details.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function gallery()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/gallery/');
				$this->db->select('*, (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as gallery');
				
				
				$event_id = $this->uri->segment(3);
				$this->db->where('id', $event_id);
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->result_array();
				if(count($data['data']) > 0)
				{
					$Media = $this->media($event_id);
					//var_dump($Media);
					$dataNew['data'] =  $Media;
					$dataNew['status'] = 'Successful';
					$dataNew['message'] = 'Events Media.';
					$this->api_model->response($this->json->encode($dataNew),200);
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function checkin()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			$event_id = $this->input->get('event_id');
			$status = $this->input->get('status');
			if($event_id)
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/gallery/');
				$this->db->select('*, 
								  DATE_FORMAT(dates, "%d-%m-%Y") as dates,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as gallery');
				
				
				$event_id = $this->input->get('event_id');
				$this->db->where('id', $event_id);
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->row_array();
				if(count($data['data']) > 0)
				{
					$user_id = $this->currentUserId->id;
					
					$this->db->where('event_id',$event_id);
					$this->db->where('user_id',$user_id);
					$q = $this->db->get('event_checkedin');
					if ( $q->num_rows() > 0 ) 
					{
						if($status == 'out')
						{
							$where = array(
							'event_id' => $event_id,
							'user_id'  => $user_id);
							$this->db->delete('event_checkedin',$where);
						}
					} else {
						if($status == 'in')
						{
							$insert = array(
							'event_id' => $event_id,
							'user_id'  => $user_id,
							'status'  => 'In',
							'created_at' => time());
							$this->db->insert('event_checkedin',$insert);
						}
					}
					
					$arraylist = $this->checkedintotal($event_id);
					$mystatuscheckin = $this->mystatuscheckin($event_id);
					//var_dump($arraylist);
					//$dataNew = array();
					//$data['data']['checkedin'] = $arraylist['users'];
					//$data['data']['checkedin-total'] = $arraylist['total'];
					/////////////////
					$totalCheckedIn = $this->checkedintotal($event_id);
					$interests = $this->interests($event_id);
					$gallery = $this->media($event_id);
					$mystatus = $this->mystatus($event_id);
					$checkin = $this->mystatuscheckin($event_id);
					$data['data']['date_time'] = date('d-m-Y h:i a', strtotime($data['data']['dates'] . ' ' . $data['data']['start_time']));
					$data['data']['mystatus'] = $mystatus;
					$data['data']['mycheckin'] = $checkin;
					$data['data']['gallery'] = $gallery;
					$data['data']['interests'] = $interests;
					$data['data']['users'] = $this->userstotal($event_id);
					$data['data']['users']['checkedin'] = $totalCheckedIn['users'];
					$data['data']['users']['checkedin-total'] = $totalCheckedIn['total'];
					/////////////////
					//$data['data']['mystatus'] = $mystatuscheckin;
					
					$data['status'] = 'Successful';
					$data['message'] = 'Events checkedin.';
					$this->api_model->response($this->json->encode($data),200);
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
			} else {
				$data['status'] = 'Error';
				$data['message'] = 'Events not found.';
				$this->api_model->response($this->json->encode($data),404);
			}
		}
	}
	
	public function attending()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			$event_id = $this->input->get('event_id');
			$status = $this->input->get('status');
			if($event_id)
			{
				$path = base_url('/uploads/');
				$profile = base_url('/events/gallery/');
				$this->db->select('*, 
								  DATE_FORMAT(dates, "%d-%m-%Y") as dates,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT count(*) FROM event_users where event_id = events.id) as member, 
													CONCAT(\''.$path. '\', photo) as imagepath,
													CONCAT(\''.$profile. '\', id) as gallery');
				
				
				$event_id = $this->input->get('event_id');
				$this->db->where('id', $event_id);
				$this->db->from('events');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->row_array();
				//var_dump($data['data']);
				$data['data']['date_time'] = date('d-m-Y h:i a', strtotime($data['data']['dates'] . ' ' . $data['data']['start_time']));
				
				if(count($data['data']) > 0)
				{
					$user_id = $this->currentUserId->id;
					
					$this->db->where('event_id',$event_id);
					$this->db->where('user_id',$user_id);
					$q = $this->db->get('event_users');
					if ( $q->num_rows() > 0 ) 
					{
						$update = array(
						'status'  => $status,
						'created_at' => time());
						$this->db->where('event_id',$event_id);
						$this->db->where('user_id',$user_id);
						$this->db->update('event_users',$update);
					} else {
						$insert = array(
						'event_id' => $event_id,
						'user_id'  => $user_id,
						'status'  => $status,
						'created_at' => time());
						$this->db->insert('event_users',$insert);
					}
					
					$arraylist = $this->userstotal($event_id);
					
					//$data = array();
					$mystatus = $this->mystatus($event_id);
					$data['data']['mystatus'] = $mystatus;
					$data['data']['users'] = $arraylist;
					$data['status'] = 'Successful';
					$data['message'] = 'Events attending.';
					$this->api_model->response($this->json->encode($data),200);
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Events not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
			} else {
				$data['status'] = 'Error';
				$data['message'] = 'Events not found.';
				$this->api_model->response($this->json->encode($data),404);
			}
		}
	}
	
	public function checkedintotal($event_id)
	{
		$arraylist = array();
		
		$this->db->where('event_id',$event_id);
		$ListTypes = $this->db->get('event_checkedin');
		$ListTypes = $ListTypes->result_array();
		if(count($ListTypes) > 0)
		{
			foreach($ListTypes as $ListTypesRow)
			{
				$ListTypesRow['user'] = $this->user_model->profile_get($ListTypesRow['user_id']);
				$ListTypesRow['user']['imagepath'] =  base_url('/uploads/') .$ListTypesRow['user']['image'];
				$ListTypesRow['user']['profilepath'] = $profile = base_url('/alumni/profile/') . $ListTypesRow['user_id'];
				$arraylist[] = $ListTypesRow; 
			}
		}
		
		$returns['users'] = $arraylist;
		$returns['total'] = count($arraylist);
		return $returns;
	}
	
	public function userstotal($event_id)
	{
		$arraylist = array();
		
		$this->db->where('event_id',$event_id);
		$ListTypes = $this->db->get('event_users');
		$ListTypes = $ListTypes->result_array();
		if(count($ListTypes) > 0)
		{
			foreach($ListTypes as $ListTypesRow)
			{
				if($ListTypesRow['status'] == 'going')
				{
					$ListTypesRow['user'] = $this->user_model->profile_get($ListTypesRow['user_id']);
					$ListTypesRow['user']['imagepath'] =  base_url('/uploads/') .$ListTypesRow['user']['image'];
					$ListTypesRow['user']['profilepath'] = $profile = base_url('/alumni/profile/') . $ListTypesRow['user_id'];
					$arraylist['going'][] = $ListTypesRow; 
				}
				
				if($ListTypesRow['status'] == 'interested')
				{
					$ListTypesRow['user'] = $this->user_model->profile_get($ListTypesRow['user_id']);
					$ListTypesRow['user']['imagepath'] =  base_url('/uploads/') .$ListTypesRow['user']['image'];
					$ListTypesRow['user']['profilepath'] = $profile = base_url('/alumni/profile/') . $ListTypesRow['user_id'];
					$arraylist['interested'][] = $ListTypesRow; 
				}
				
				if($ListTypesRow['status'] == 'not-interested')
				{
					$ListTypesRow['user'] = $this->user_model->profile_get($ListTypesRow['user_id']);
					$ListTypesRow['user']['imagepath'] =  base_url('/uploads/') .$ListTypesRow['user']['image'];
					$ListTypesRow['user']['profilepath'] = $profile = base_url('/alumni/profile/') . $ListTypesRow['user_id'];
					$arraylist['not-interested'][] = $ListTypesRow; 
				}
			}
		}
		
		$arraylist['interested-total'] = count($arraylist['interested']);
		$arraylist['not-interested-total'] = count($arraylist['not-interested']);
		$arraylist['going-total'] = count($arraylist['going']);
		return $arraylist;
	}
	
	public function media($event_id)
	{
		$arraylist = array();
		
		$this->db->where('event_id',$event_id);
		$ListTypes = $this->db->get('event_gallery');
		$ListTypes = $ListTypes->result_array();
		if(count($ListTypes) > 0)
		{
			foreach($ListTypes as $ListTypesRow)
			{
				if($ListTypesRow['type'] == 'photo')
				{
					$ListTypesRow['imagepath'] =  base_url('/uploads/') .$ListTypesRow['path'];
					$arraylist['photos'][] = $ListTypesRow; 
				} 
				
				if($ListTypesRow['type'] == 'video')
				{
					$ListTypesRow['videopath'] =  $ListTypesRow['path'];
					$arraylist['videos'][] = $ListTypesRow; 
				}
			}
		}
		
		$arraylist['photo-total'] = count($arraylist['photos']);
		$arraylist['video-total'] = count($arraylist['videos']);
		return $arraylist;
	}
	
	public function interests($event_id)
	{
		$arraylist = array();
		
		//$this->select('interests.*, event_interests.*');
		$this->db->where('event_id',$event_id);
		$this->db->where('interests.id = event_interests.interest_id');
		$ListTypes = $this->db->get('event_interests,interests');
		$ListTypes = $ListTypes->result_array();
		/*if(count($ListTypes) > 0)
		{
			foreach($ListTypes as $ListTypesRow)
			{
				//$ListTypesRow['interest'] =  $ListTypesRow['path'];
				$arraylist['interests'][] = $ListTypesRow;
			}
		}*/
		
		//$arraylist['photo-total'] = count($arraylist['photos']);
		//$arraylist['video-total'] = count($arraylist['videos']);
		return $ListTypes;
	}
	
	function mystatus($event_id)
	{
		$user_id = $this->currentUserId->id;
		$this->db->where('user_id',$user_id);
		$this->db->where('event_id',$event_id);
		$ListTypes = $this->db->get('event_users');
		$ListTypes = $ListTypes->row_array();
		$status = 'default';
		if($ListTypes)
		{
			$status = $ListTypes['status']; 
		}
		return $status ;
	}
	
	function mystatuscheckin($event_id)
	{
		$user_id = $this->currentUserId->id;
		$this->db->where('user_id',$user_id);
		$this->db->where('event_id',$event_id);
		$ListTypes = $this->db->get('event_checkedin');
		$ListTypes = $ListTypes->row_array();
		$status = 'default';
		if($ListTypes)
		{
			$status = $ListTypes['status']; 
		}
		return $status ;
	}
}
?>