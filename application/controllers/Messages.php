<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Messages Api

require('Api.php');

class Messages extends Api 
{
	public $content_type = "application/json";
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('messages_model');
	}

	public function index()
	{
		
	}
	
	public function all()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			
			$type = $this->input->get('type');
			$paged = $this->input->get('paged');
			
			$read = $this->threads('read',$paged,'yes');
			$unread = $this->threads('unread',$paged,'yes');
			$archive = $this->threads('archive',$paged,'yes');
			
			$data['messages']['read'] = $read;
			$data['messages']['unread'] = $unread;
			$data['messages']['archive'] = $archive;
			$data['status'] = 'Successful';
			$data['message'] = 'Threads list.';
			$this->api_model->response($this->json->encode($data),200);
		}
	}

	public function threads($segment = NULL, $pagenumber = NULL, $output = NULL)
	{
		//echo $pagenumber;
		//var_dump($this->currentUserId->id);
		/*if($output)
		{
			$roomStatus = $output;
		} else {
			$roomStatus = $this->uri->segment(3);
		}*/
		if($segment)
		{
			$roomStatus = $segment;
		} else {
			$roomStatus = $this->uri->segment(3);
		}
		
		//$roomStatus = $this->uri->segment(3);
		
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$messages = base_url('/messages/messageboard/');
				//$this->db->select('*, CONCAT(\''.$path. '\', profile.image) as imagepath, CONCAT(\''.$profile. '\', profile.user_id) as profilepath');
				$this->db->select('*, 
								  CONCAT(\''.$messages. '\', id) as allmessages,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at,
								  (SELECT message from chat_messages where chatroom_id = chats.id order by -id limit 1) as latest_message,
								  (SELECT  IF(receiver_id = '.$this->currentUserId->id.' , 1, 2) ) as sender');
				
				//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
				$this->db->group_start();
				$this->db->or_where('receiver_id',$this->currentUserId->id);
				$this->db->or_where('sender_id',$this->currentUserId->id);
				$this->db->group_end();
				
				/*$this->db->group_start();
				$this->db->or_where('chats.receiver_id = profile.user_id');
				$this->db->or_where('chats.sender_id = profile.user_id');
				$this->db->group_end();*/
				//$this->db->where('users.status', "active");
				//profile table name
				$this->db->order_by('updated_at','DESC');
				
				if($roomStatus == 'archive')
				{
					$this->db->group_start();
					$this->db->or_where('archive', $this->currentUserId->id);
					$this->db->or_where('archive', 'both');
					$this->db->group_end();
				} else if($roomStatus)
				{
					if($roomStatus == 'unread')
					{
						$this->db->where('status', $this->currentUserId->id);	
					}
					$this->db->where('archive', '');
					
				} else {
					$this->db->where('archive', '');
				}
				
				$this->db->from('chats');
				
				$limit = 10;
				/*if($pagenumber)
				{
					$paged = $pagenumber;
				} else {
					$paged = $this->input->get('paged');
				}*/
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;

				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes' or $output == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
								
				$data['data'] = $data2->result_array();
				$NewData = array();
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						if($row['receiver_id'] != $this->currentUserId->id)
						{
							$row['profile'] =  $this->user_model->profile_get($row['receiver_id']);
							$row['profile']['image'] =  base_url('/uploads/') . $row['profile']['image'];
						}
						
						if($row['sender_id'] != $this->currentUserId->id)
						{
							$row['profile'] =  $this->user_model->profile_get($row['sender_id']);
							$row['profile']['image'] =  base_url('/uploads/') . $row['profile']['image'];
						}
						if($roomStatus == 'archive')
						{
							if($row['status'] == $this->currentUserId->id)
							{
								$row['status'] = 'unread';
							} else {
								$row['status'] = 'read';
							}
						} else {
							$row['status'] = $roomStatus;
						}
					}
				}
				
				//$data['data'] = $NewData;
				if($output == 'yes')
				{
					return $data['data'];
				}
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Threads list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Threads not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function messageboard()
	{
		//$this->currentUserId->id;
		$chatRoomID = $this->uri->segment(3);
		//$this->messages_model->updatedthreadsstatus($chatRoomID,'read');
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$messages = base_url('/messages/messageboard/');
				//$this->db->select('*, CONCAT(\''.$path. '\', profile.image) as imagepath, CONCAT(\''.$profile. '\', profile.user_id) as profilepath');
				$this->db->select('*,
								  from_unixtime(created_at, "%d-%m-%Y %H:%i %p") as created_at,
								  from_unixtime(updated_at, "%d-%m-%Y %H:%i %p") as updated_at');
				
				//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
				//$this->db->group_start();
				//$this->db->or_where('sender_id',$this->currentUserId->id);
				//$this->db->group_end();
				$this->db->or_where('chatroom_id',$chatRoomID);
				$this->db->order_by('id','DESC');
				$this->db->from('chat_messages');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'threads list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Messages not found.';
					$this->api_model->response($this->json->encode($data),404);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function messagesent()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST')
        {
			$data['status'] = 'Error';
			$data['message'] = 'Wrong mrthod.';
			$this->api_model->response($this->json->encode($data),406);
		} else {
			$this->form_validation->set_rules('message', 'Message', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">�</a><strong>', '</strong></div>');
            
			//if the form has passed through the validation
            if ($this->form_validation->run())
            {
			
				$user_id = $this->currentUserId->id;
				$chatRoomID = $this->input->post('room_id');
				$message = $this->input->post('message');
				
				$submit = array();
				$submit['chatroom_id'] = $chatRoomID;
				$submit['sender_id'] = $user_id;
				$submit['message'] = $message;
				$submit['created_at'] = time();
				$return = $this->messages_model->create($submit);
				
				// get receiver_id
				$receiverid = $this->chatroom_receiver($user_id, $chatRoomID);
			
				// create notification
				$this->notification->createnotification($receiverid,$user_id,'message',$chatRoomID);
				
				$this->messages_model->updatedthreadsstatus($chatRoomID, $receiverid);
				if($return > 0)
				{
					$this->messages_model->update_chatroom_time($chatRoomID);
					$data['message_id'] = $return;
					$data['status'] = 'Successful';
					$data['message'] = 'Your message was sent successfully.';
					$this->api_model->response($this->json->encode($data),200);
				}
			} else {
				//$this->load->view('errors');
				$data['status'] = 'Error';
				$data['message'] = 'Messsage are required.';
				$this->api_model->response($this->json->encode($data),406);
			}
		}
	}
	
	public function tindermessage()
	{
		$sender_id = $this->currentUserId->id;
		$receiver_id = $this->input->post('receiver_id');
		$message = $this->input->post('message');
		
		$room = $this->checkchannel($sender_id, $receiver_id);
		
		if($room)
		{
			$chatRoomID = $room['id'];
			
			// get receiver_id
			$receiverid = $this->chatroom_receiver($sender_id, $chatRoomID);
			
			$this->messages_model->updatedthreadsstatus($chatRoomID,$receiverid);
			
			// create notification
			$this->notification->createnotification($receiverid,$sender_id,'tinder',$chatRoomID);
			
			$submit = array();
			$submit['chatroom_id'] = $chatRoomID;
			$submit['sender_id'] = $sender_id;
			$submit['message'] = $message;
			$submit['type'] = 'tinder';
			$submit['created_at'] = time();
			$return = $this->messages_model->create($submit);
			
			if($return > 0)
			{
				$this->messages_model->update_chatroom_time($chatRoomID);
				$data['message_id'] = $return;
				$data['status'] = 'Successful';
				$data['message'] = 'Your message was sent successfully.';
				$this->api_model->response($this->json->encode($data),200);
			}
		} else {
			$this->createchannel($sender_id,$receiver_id);
		}
	}
	
	public function sayhimessage()
	{
		$sender_id = $this->currentUserId->id;
		$receiver_id = $this->input->post('receiver_id');
		$message = $this->input->post('message');
		
		$room = $this->checkchannel($sender_id, $receiver_id);
		
		if($room)
		{
			$chatRoomID = $room['id'];
			
			$submit = array();
			$submit['chatroom_id'] = $chatRoomID;
			$submit['sender_id'] = $sender_id;
			$submit['message'] = $message;
			$submit['created_at'] = time();
			$return = $this->messages_model->create($submit);
			
			// get receiver_id
			$receiverid = $this->chatroom_receiver($sender_id, $chatRoomID);
			
			// create notification
			$this->notification->createnotification($receiverid,$sender_id,'message',$chatRoomID);
			
			$this->messages_model->updatedthreadsstatus($chatRoomID, $receiverid);
			if($return > 0)
			{
				$this->messages_model->update_chatroom_time($chatRoomID);
				$data['message_id'] = $return;
				$data['status'] = 'Successful';
				$data['message'] = 'Your message was sent successfully.';
				$this->api_model->response($this->json->encode($data),200);
			}
		} else {
			$roomID = $this->createchannel($sender_id,$receiver_id);
			
			$chatRoomID = $roomID;
			
			$submit = array();
			$submit['chatroom_id'] = $chatRoomID;
			$submit['sender_id'] = $sender_id;
			$submit['message'] = $message;
			$submit['created_at'] = time();
			$return = $this->messages_model->create($submit);
			
			// get receiver_id
			$receiverid = $this->chatroom_receiver($sender_id, $chatRoomID);
			
			// create notification
			$this->notification->createnotification($receiverid,$sender_id,'message',$chatRoomID);
			
			$this->messages_model->updatedthreadsstatus($chatRoomID, $receiverid);
			if($return > 0)
			{
				$this->messages_model->update_chatroom_time($chatRoomID);
				$data['message_id'] = $return;
				$data['status'] = 'Successful';
				$data['message'] = 'Your message was sent successfully.';
				$this->api_model->response($this->json->encode($data),200);
			}
		}
	}
	
	function checkchannel($sender_id,$receiver_id)
	{
		$this->db->select('*');
		$this->db->group_start();
		
		$this->db->group_start();
		$this->db->or_where('receiver_id',$this->currentUserId->id);
		$this->db->or_where('sender_id',$this->currentUserId->id);
		$this->db->group_end();
		
		$this->db->group_start();
		$this->db->or_where('receiver_id',$receiver_id);
		$this->db->or_where('sender_id',$receiver_id);
		$this->db->group_end();
		
		$this->db->group_end();
		
		$this->db->from('chats');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->row_array(); 
	}
	
	function createchannel($sender_id,$receiver_id)
	{
		$submit = array();
		$submit['sender_id'] = $sender_id;
		$submit['receiver_id'] = $receiver_id;
		$submit['created_at'] = time();
		$this->db->insert('chats', $submit);
		return $this->db->insert_id();
	}
	
	function archive()
	{
		$user_id = $this->currentUserId->id;
		$chatRoomID = $this->uri->segment(3);
		$status = $this->input->get('status');
		$this->messages_model->updatedarchive($chatRoomID,$user_id,$status);
		
		$data['status'] = 'Successful';
		$data['message'] = 'Thread '.$status.'.';
		$this->api_model->response($this->json->encode($data),200);
	}
	
	public function read()
	{
		$user_id = $this->currentUserId->id;
		$chatRoomID = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->where('id',$chatRoomID);
		$this->db->from('chats');
		$query = $this->db->get();
		$query = $query->row_array();
		if($query['status'] == $user_id)
		{
			$this->messages_model->updatedthreadsstatus($chatRoomID,'read');
		}
		$data['status'] = 'Successful';
		$data['message'] = 'Thread read.';
		$this->api_model->response($this->json->encode($data),200);
	}
	
	public function chatroom_receiver($user_id, $chatroom_id)
	{
		$user_id = $user_id;
		$chatRoomID = $chatroom_id;
		$this->db->select('*');
		$this->db->where('id',$chatRoomID);
		$this->db->from('chats');
		$query = $this->db->get();
		$query = $query->row_array();
		if($query['receiver_id'] == $user_id)
		{
			return $query['sender_id'];
		}
		if($query['sender_id'] == $user_id)
		{
			return $query['receiver_id'];
		}
	}
	
	public function tinder()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST')
        {
			$this->api_model->response('',406);
		} else {
			$sender_id = $this->currentUserId->id;
			
			$this->input->post('user_id');
			$this->input->post('status'); //Accept - Reject - Later
			$this->input->post('message');
			
			//var_dump($_POST);
			
			$data['status'] = 'Successful';
			$data['message'] = 'Tinder updated.';
			$this->api_model->response($this->json->encode($data),200);
		}
	}
}
?>