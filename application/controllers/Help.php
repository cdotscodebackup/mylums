<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Help extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
	
	public function desk_post()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('roll_number','Roll Number', 'required');
		$this->form_validation->set_rules('first_name','First Name','required');
		$this->form_validation->set_rules('last_name','Last Name','required');
		$this->form_validation->set_rules('phone_number','Phone Name','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		if($this->form_validation->run() == true)
		{
			$dataPost = $this->input->post();
			
			$this->notification->help_desk($dataPost);
			
			$response['status'] = 'Successful';
			$response['message'] = 'Your email has been sent.';
			$this->set_response($response, REST_Controller::HTTP_OK);
		} else {
			$response['status'] = 'Error';
			$response['message'] = 'Information not correct.';
			$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
			return;
		}
	}
	
	public function menu_get()
	{
		$response['content'] = $this->pages(1);
		$response['data'] = $this->help_list();
		$response['status'] = 'Successful';
		$response['message'] = 'Help Menu.';
		$this->set_response($response, REST_Controller::HTTP_OK);	
	}
	
	public function pages($page_id = NULL)
	{
		$path = base_url('/uploads/');
		$profile = base_url('/events/gallery/');
		$this->db->select('*');
		if($page_id)
		{
			$this->db->where("id",$page_id);
		}
		$ListTypes = $this->db->get('pages');
		
		if($page_id)
		{
			$ListTypes = $ListTypes->row_array();
		} else {
 			$ListTypes = $ListTypes->result_array();
		}
		return $ListTypes;
	}
	
	public function help_list()
	{	
		$path = base_url('/uploads/');
		$profile = base_url('/events/gallery/');
		$this->db->select('*, CONCAT(\''.$path. '\', image_active) as imagepath');
		$this->db->order_by('list_order');
		$ListTypes = $this->db->get('help_menu');
		$ListTypes = $ListTypes->result_array();
		return $ListTypes;
	}
}