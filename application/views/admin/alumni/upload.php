<?php /*?><div class="content bg-gray-lighter">
    <div class="row items-push">
      <div class="col-sm-7">
        <h1 class="page-heading"> Form Validation <small>It’s never been easier to validate form values.</small> </h1>
      </div>
      <div class="col-sm-5 text-right hidden-xs">
        <ol class="breadcrumb push-10-t">
          <li>Forms</li>
          <li><a class="link-effect" href="">Validation</a></li>
        </ol>
      </div>
    </div>
  </div><?php */?>

<div class="content">
  <div class="row">
      <ul class="breadcrumb">
        <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
        <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
        <li class="active"> <a href="#">New</a> </li>
      </ul>
      <div class="page-header">
        <h2> Adding <?php echo ucfirst($this->uri->segment(2));?> </h2>
      </div>
    
  </div>
</div>
<div class="col-lg-12">
      <div class="block">
        <div class="block-header"> </div>
        <div class="block-content block-content-narrow">
          <?php
	  //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new category created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
          <?php
		//form data
		$attributes = array('class' => 'js-validation-bootstrap form-horizontal', 'id' => '', 'enctype' => 'multipart/form-data');
		//form validation
		echo validation_errors();
		echo form_open_multipart('admin/alumni/upload', $attributes);
      ?>
          <?php /*?><form class="js-validation-bootstrap form-horizontal" action="/admin/products/add" enctype="multipart/form-data" method="post"><?php */?>
            
            <div class="form-group">
              <label class="col-md-3 control-label" for="InputFile">Upload file</label>
              <div class="col-md-8">
                <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
              </div>
            </div>
            
            <?php echo $file; //var_dump($file); ?>
            
            <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
              <input type="hidden" name="fileuploading" value="yes" />
                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                <button class="btn btn-sm btn-primary" type="reset">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

