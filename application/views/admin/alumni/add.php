<?php /*?><div class="content bg-gray-lighter">
    <div class="row items-push">
      <div class="col-sm-7">
        <h1 class="page-heading"> Form Validation <small>It’s never been easier to validate form values.</small> </h1>
      </div>
      <div class="col-sm-5 text-right hidden-xs">
        <ol class="breadcrumb push-10-t">
          <li>Forms</li>
          <li><a class="link-effect" href="">Validation</a></li>
        </ol>
      </div>
    </div>
  </div><?php */?>

<div class="content">
  <div class="row">
      <ul class="breadcrumb">
        <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
        <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
        <li class="active"> <a href="#">New</a> </li>
      </ul>
      <div class="page-header">
        <h2> Adding <?php echo ucfirst($this->uri->segment(2));?> </h2>
      </div>
    
  </div>
</div>
<div class="col-lg-12">
      <div class="block">
        <div class="block-header"> </div>
        <div class="block-content block-content-narrow">
          <?php
	  //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new category created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
          <?php
		//form data
		$attributes = array('class' => 'js-validation-bootstrap form-horizontal', 'id' => '');
		//form validation
		echo validation_errors();
		echo form_open_multipart('admin/products/add', $attributes);
      ?>
          <?php /*?><form class="js-validation-bootstrap form-horizontal" action="/admin/products/add" enctype="multipart/form-data" method="post"><?php */?>
            <div class="form-group">
              <label class="col-md-3 control-label" for="val-username">Name <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <input class="form-control" type="text" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Choose a nice name..">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="val-suggestions">Description <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <textarea class="form-control" id="description" name="description" rows="18" placeholder=""><?php echo set_value('description'); ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="parent">Category</label>
              <div class="col-md-7">
                <select name="category" id="parent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                  <option value="0" selected="selected">Select</option>
                  <?php 
				if(count($allcategories) > 0)
				{
					foreach($allcategories as $category)
					{
						echo '<option value="'.$category['ID'].'">'.$category['C_Name'].'</option>';
					}
				} ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="featured">Featured</label>
              <div class="col-md-7">
                <select name="featured" id="featured" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                  <option value="0" selected="selected">No</option>
                  <option value="1" >Yes</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="InputFile">Ringtone<br />
                Wallpaper</label>
              <div class="col-md-8">
                <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="InputDownload">Downloadable</label>
              <div class="col-md-8">
                <input type="file" class="form-control-file" id="InputDownload" name="download" aria-describedby="fileHelp">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                <button class="btn btn-sm btn-primary" type="reset">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

