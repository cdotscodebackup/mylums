<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
      <li class="active"> <a href="#">Update</a> </li>
    </ul>
    <div class="page-header">
      <h2> Updating <?php echo ucfirst($this->uri->segment(2));?> </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
    <div class="block">
    	<div class="block-header"> </div>
        <div class="block-content block-content-narrow">
        	<?php
          //flash messages
          if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
              echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> '.ucfirst($this->uri->segment(2)).' updated with success.';
              echo '</div>';       
            }else{
              echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
              echo '</div>';          
            }
          }
          ?>
    <?php
          //form data
          $attributes = array('class' => 'form-horizontal', 'id' => '');
    
          //form validation
          echo validation_errors();
          echo form_open_multipart('admin/'.$this->uri->segment(2).'/update/'.$this->uri->segment(4).'', $attributes);
          ?>
          <?php if(count($columnsEdit) > 0) { ?>
          <?php $feildCounter = 0; ?>
                    <?php foreach($columnsEdit as $column){ ?>
                    	
                        	<div class="form-group">
              <label class="col-md-3 control-label" for="val-username"><?php echo $column['title']; ?><?php if($column['status'] != ""){ ?> <span class="text-danger">*</span> <?php } ?></label>
              <div class="col-md-8">
                <input class="form-control" type="text" id="<?php echo $column['field']; ?>" name="<?php echo $column['field']; ?>" value="<?php 
				echo ($column['database'] !="") ? $manufacture[0][$column['database']] : ''; ?>" placeholder="<?php echo $column['title']; ?>" <?php 
				echo ($column['type'] =="disable") ? 'disabled' : ''; ?>>
              </div>
            </div>
            			
            <?php $feildCounter++; ?>
                    
                    <?php } ?>
                    <?php } ?>
       <?php /*?>   
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-username">Name <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <input class="form-control" type="text" id="name" name="name" value="<?php echo $manufacture[0]['P_Name']; ?>" placeholder="Choose a nice name..">
              </div>
            </div>
          
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-suggestions">Description <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <textarea class="form-control" id="description" name="description" rows="18" placeholder=""><?php echo $manufacture[0]['P_Description']; ?></textarea>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-3 control-label" for="parent">Category</label>
              <div class="col-md-7">
                <select name="category" id="parent">
            <option value="0" selected="selected">Select</option>
            <?php 
                    if(count($allcategories) > 0)
                    {
                        foreach($allcategories as $category)
                        {
                            if($manufacture[0]['ID'] != $category['ID']){
                                echo '<option value="'.$category['ID'].'">'.$category['C_Name'].'</option>';
                            }
                        }
                    } ?>
          </select>
          <script type="text/javascript">document.getElementById('parent').value = "<?php echo $manufacture[0]['CatID']; ?>"</script>
              </div>
            </div>
    
    <div class="form-group">
              <label class="col-md-3 control-label" for="parent">Featured</label>
              <div class="col-md-7">
                <select name="featured" id="featured" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                  <option value="0" selected="selected">No</option>
                  <option value="1" >Yes</option>
                </select>
          <script type="text/javascript">document.getElementById('featured').value = "<?php echo $manufacture[0]['P_Featured']; ?>"</script>
              </div>
            </div>
      
      
      <div class="form-group">
      <label class="col-md-3 control-label" for="parent">Ringtone<br />Wallpaper</label>
        
        <div class="col-md-7">
          <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
          <?php 
                    if($manufacture[0]['P_Path'])
                    {
        echo '<img src="'.base_url().'uploads/'.$manufacture[0]['P_Path'].'" name="'.$manufacture[0]['P_Name'].'" alt="'.$manufacture[0]['P_Name'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?>
        </div>
        
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="parent">Downloadable</label>
        <div class="col-md-7">
          <input type="file" class="form-control-file" id="InputDownload" name="download" aria-describedby="fileHelp">
          <?php 
                    if($manufacture[0]['P_Downloadable'])
                    {
        echo '<img src="'.base_url().'uploads/'.$manufacture[0]['P_Downloadable'].'" name="'.$manufacture[0]['P_Name'].'" alt="'.$manufacture[0]['P_Name'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?>
        </div>
        
      </div><?php */?>
      
      
      <?php /*?><div class="form-group">
      <label class="col-md-3 control-label" for="parent">Profile Image</label>
        
        <div class="col-md-7">
          <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
          <?php 
                    if($manufacture[0]['image'])
                    {
        echo '<img src="'.base_url().'uploads/'.$manufacture[0]['image'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?>
        </div>
        
      </div><?php */?>
      
      <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
        <button class="btn btn-primary" type="submit">Save changes</button>
        <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"><button class="btn" type="button">Cancel</button></a>
      </div>
            </div>
    <?php echo form_close(); ?>
    	</div>
    </div> 
</div>