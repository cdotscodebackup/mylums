<style>
.customScrollTable
{
	overflow:hidden;
}
.customScrollTable table {
    width: 100%;
    max-width: 100%;
}
</style>
<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li class="active"> <?php echo ucfirst($this->uri->segment(2));?> </li>
    </ul>
    <div class="page-header users-header">
      <h2> <?php echo ucfirst($this->uri->segment(2));?> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add a new</a> </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <!-- Striped Table -->
  <div class="block">
    <?php /*?><div class="block-header">
        <div class="block-options"> <code>.table-striped</code> </div>
        <h3 class="block-title">Striped Table</h3>
      </div><?php */?>
    <div class="block-content">
      <div class="well">
           
            <?php
           
				$attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
				
				//save the columns names in a array that we will use as filter         
				$options_manufacturers = array();    
				foreach ($manufacturers as $array) 
				{
					foreach ($array as $key => $value) 
					{
						$options_manufacturers[$key] = $key;
					}
					break;
				}
				echo form_open('admin/'.$this->uri->segment(2), $attributes);
				
				echo form_label('Search:', 'search_string');
				echo form_input('search_string', $search_string_selected); 
				//echo form_dropdown('category', $CatList, $category, 'class="span2"');
				
				echo form_label('Search by:', 'searchField');
				echo form_dropdown('searchField', $columns, $searchField, 'class="span2"');
				
				
				echo form_label('Order by:', 'order');
				echo form_dropdown('order', $columns, $order, 'class="span2"');
				
				$data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');
				
				$options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
				echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');
				
				echo form_submit($data_submit);
				
				echo form_close();
            ?>

          </div>
      <div class="customScrollTable"><table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th class="header">#</th>
            <th class="yellow header headerSortDown">Photo</th>
            <?php if(count($columns) > 0) { ?>
            <?php foreach($columns as $column => $vals){ ?>
            	<th class="yellow header headerSortDown"><?php echo $vals; ?></th>
            <?php } ?>
            <?php } ?>
            <th class="yellow header headerSortDown">Offers</th>
            <td></td>
          </tr>
        </thead>
        <tbody>
          <?php if(count($manufacturers) > 0){ ?>
          		<?php foreach($manufacturers as $row){ ?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php if($row['image']) 
					{
						echo '<img src="'.base_url().'uploads/'.$row['image'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?></td>
                    <?php if(count($columns) > 0) { ?>
                    <?php foreach($columns as $column => $vals){ ?>
                    <td><?php echo $row[$column]; ?></th>
                    <?php } ?>
                    <?php } ?>
                    <td><?php echo $row['offers']; ?></td>
                    <td><div class="btn-group">
                  <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['id']; ?>" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                  <a href="#." onclick="confirmDelete('<?php echo site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['id']; ?>')" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></a>
               </div></td>
                </tr>
				<?php } ?>
          <?php } ?>
          
            <?php  /*foreach($manufacturers as $row)
              {
                echo '<tr>';
                echo '<td>'.$row['ID'].'</td>';
				if($row['P_Path'])
				{
					echo '<td><img src="'.base_url().'uploads/'.$row['P_Path'].'" name="'.$row['P_Name'].'" alt="'.$row['P_Name'].'" width="100" /></td>';
				} else {
					echo '<td></td>';
				}
				echo '<td>'.$row['P_Name'].'</td>';
				
				//echo '<td>'. ucfirst($row['P_Name']).'</td>';
				echo '<td>';
				if($row['CatID'] > 0)
				{
					$Parent = $this->categories_model->get_manufacture_by_id($row['CatID']);
					//var_dump($Parent);
					echo ($Parent) ? $Parent[0]['C_Name'] : "";
				}
				echo '</td>';
				
				echo '<td><a href="'.site_url("api").'/forcedownload/?file='.$row['ID'].'">Download</a></td>';
				echo '<td>'.$row['P_Count'].'</td>';
				
                echo '<td class="text-center"><div class="btn-group">
                  <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['ID'].'" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                  <a href="#." onclick="confirmDelete(\''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['ID'].'\')" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></a>
               </div> </td>';
                echo '</tr>';
              }*/
              ?>
        </tbody>
      </table></div>
    </div>
    <div class="block-content">
      <nav class="text-right"> <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?> </nav>
    </div>
  </div>
</div>
<?php /*?></div><?php */?>