<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
      <li class="active"> <a href="#">New Email</a> </li>
    </ul>
    <div class="page-header">
      <h2> <?php echo ucfirst($this->uri->segment(2));?></h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
    <div class="block">
    	<div class="block-header"> </div>
        <div class="block-header">
        	<?php
          //flash messages
          if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
              echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> '.ucfirst($this->uri->segment(2)).' sent with success.';
              echo '</div>';       
            }else{
              echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
              echo '</div>';          
            }
          }
          ?>
    	 <?php
          //form data
          $attributes = array('class' => 'form-horizontal', 'id' => 'submitgallery');
    
          //form validation
          echo validation_errors();
          echo form_open_multipart('admin/'.$this->uri->segment(2).'/index/', $attributes); ?>
          
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-username">Subject <span class="text-danger">*</span> </label>
              <div class="col-md-8">
              	<input class="form-control" type="text" id="" name="subject" value="" placeholder="Subject" >
                </div>
            </div>
          
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-username">Message</label>
              <div class="col-md-8">
                	<textarea name="message" class="form-control" id="" placeholder="Message" style="height:150px;"></textarea>
              </div>
            </div>
        <div class="customScrollTable"><table id="datatable" class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <td><input id="all" type="checkbox" /></td>
            <th class="header">#</th>
            <th class="yellow header headerSortDown">Photo</th>
            <th class="yellow header headerSortDown">Name</th>
            <th class="yellow header headerSortDown">Email</th>
            <th class="yellow header headerSortDown">Program</th>
            <th class="yellow header headerSortDown">Year</th>
            <th class="yellow header headerSortDown">Industry</th>
            <th class="yellow header headerSortDown">Company</th>
            <th class="yellow header headerSortDown">Designation</th>
            <th class="yellow header headerSortDown">Country</th>
            <th class="yellow header headerSortDown">State</th>
            <th class="yellow header headerSortDown">City</th>
          </tr>
        </thead>
        <tbody>
          <?php if(count($users) > 0){ ?>
          		<?php foreach($users as $row){ ?>
                <tr>
                	<td><?php 
					//$status = $controller->get_invited($row['user_id'], $manufacture[0]['id']); 
					?><div class="btn-group"><input type="checkbox" name="users[]" value="<?php echo $row['user_id']; ?>" class="checkbox" /></div></td>
                    <td><?php echo $row['user_id']; ?></td>
                    <td><?php if($row['image']) 
					{
						echo '<img src="'.base_url().'uploads/'.$row['image'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?></td>
                    <td><?php echo $row['first_name']; ?> <?php echo $row['first_name']; ?></td>
                    <td><?php echo $row['email']; ?></td>
                    
                    
                    <td><?php echo $row['program']; ?></td>
                    <td><?php echo $row['year']; ?></td>
                    <td><?php echo $row['current_industry']; ?></td>
                    <td><?php echo $row['current_company']; ?></td>
                    <td><?php echo $row['current_designation']; ?></td>
                    <td><?php echo $row['country']; ?></td>
                    <td><?php echo $row['state_province']; ?></td>
                    <td><?php echo $row['city']; ?></td>
                    
                    
                </tr>
				<?php } ?>
          <?php } ?>
          
           
        </tbody>
      </table></div>
      
      <div class="block-header">  
      <div class="form-groups">
              <div class="col-md-8 col-md-offset-4">
        <button class="btn btn-primary" type="submit">Send</button>
        <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"><button class="btn" type="button">Cancel</button></a>
      </div>
            </div>
            </div>
    <?php echo form_close(); ?>
    	</div>
    </div> 
</div>
<iframe id="galleryUpload" name="galleryUpload" style="display:none;"></iframe>
<?php //$this->load->view('map'); ?>
<?php
function quickYouTubeId($youtubeurl) {
    preg_match("#[a-zA-Z0-9-]{11}#", $youtubeurl, $id);
    return (strlen($id[0])==11) ? $id[0] : false;
}
?>
<link rel="stylesheet" id="dataTables" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready( function () 
	{
    	jQuery('#datatable').DataTable({ paging: false });
		jQuery('#all').click(function ()
		{
			status = jQuery(this).is(":checked");
			//alert(status);
			jQuery('.checkbox').each(function ()
			{
				if(status)
				{
					jQuery(this).prop( "checked", true );
				} else {
					jQuery(this).prop('checked', false).removeAttr('checked');
				};
			});
		});
	});
	function pageLoad()
	{
		$('#submitgallery').load(document.URL +  ' #submitgallery');
	}
	
	function confirmDeletes(locations) 
	{ 
		//alert(locations);
		
		var status = confirm("Are you sure you want to delete?");   
		if(status)
		{
			//return false;
			//window.location = locations;
			$('#submitgallery').load(locations +  ' #submitgallery');
			//parent.location.replace("parent.location='<?php echo "delete.php?id=" . $people['id'] ?>'");
		}
	}
	
</script>