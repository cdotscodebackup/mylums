<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
      <li class="active"> <a href="#">Update</a> </li>
    </ul>
    <div class="page-header">
      <h2> Updating <?php echo ucfirst($this->uri->segment(2));?> </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
    <div class="block">
    	<div class="block-header"> </div>
        <div class="block-content block-content-narrow">
        	<?php
          //flash messages
          if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
              echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> Application updated with success.';
              echo '</div>';       
            }else{
              echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
              echo '</div>';          
            }
          }
          ?>
    <?php
          //form data
          $attributes = array('class' => 'form-horizontal', 'id' => '');
    
          //form validation
          echo validation_errors();
    
          echo form_open_multipart('admin/'.$this->uri->segment(2).'/update/'.$this->uri->segment(4).'', $attributes);
          ?>
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-username">Name <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <input class="form-control" type="text" id="name" name="name" value="<?php echo $manufacture[0]['A_Name']; ?>" placeholder="Choose a nice name..">
              </div>
            </div>
          
          <div class="form-group">
              <label class="col-md-3 control-label" for="val-suggestions">Description <span class="text-danger">*</span></label>
              <div class="col-md-8">
                <input name="description" type="text" class="form-control" id="description" value="<?php echo $manufacture[0]['A_Package']; ?>" placeholder="" />
              </div>
            </div>
            
            
    
    
      
      
      <div class="form-group">
      <label class="col-md-3 control-label" for="parent">Icon</label>
        
        <div class="col-md-7">
          <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
          <?php 
                    if($manufacture[0]['A_Icon'])
                    {
        echo '<img src="'.base_url().'uploads/'.$manufacture[0]['A_Icon'].'" name="'.$manufacture[0]['A_Icon'].'" alt="'.$manufacture[0]['A_Icon'].'" width="100" />';
                    } else {
                        //echo '<td></td>';
                    }?>
        </div>
        
      </div>
      
      <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
        <button class="btn btn-primary" type="submit">Save changes</button>
        <button class="btn" type="reset">Cancel</button>
      </div>
            </div>
    <?php echo form_close(); ?>
    	</div>
    </div> 
</div>