<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
      <li class="active"> <a href="#">New</a> </li>
    </ul>
    <div class="page-header">
      <h2> Adding <?php echo ucfirst($this->uri->segment(2));?> </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="block">
    <div class="block-header"> </div>
    <div class="block-content block-content-narrow">
      <?php
	  //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new category created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();
      
      echo form_open_multipart('admin/categories/add', $attributes);
      ?>
      <div class="form-group">
        <label for="inputError" class="col-md-3 control-label" >Name</label>
        <div class="col-md-8">
          <input type="text" class="form-control" id="" name="name" value="<?php echo set_value('name'); ?>" >
          <!--<span class="help-inline">Woohoo!</span>-->
        </div>
      </div>
      <div class="form-group">
        <label for="inputError" class="col-md-3 control-label" >Type</label>
        <div class="col-md-8">
          <?php /*?><input type="text" id="" name="type" value="<?php echo set_value('type'); ?>" ><?php */?>
          <select name="type" id="types" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
            <option value="ringtone" selected="selected">Ringtone</option>
            <option value="wallpaper">Wallpaper</option>
          </select>
          <?php /*?><script type="text/javascript">document.getElementById('types').value = "<?php echo set_value('type'); ?>"</script><?php */?>
        </div>
      </div>
      <div class="form-group">
        <label for="inputError" class="col-md-3 control-label" >Parent</label>
        <div class="col-md-8">
          <?php //var_dump($allcategories); ?>
          <select name="parent" id="parent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
            <option value="1" selected="selected">Select</option>
            <?php 
				if(count($allcategories) > 0)
				{
					foreach($allcategories as $category)
					{
						echo '<option value="'.$category['ID'].'">'.$category['C_Name'].'</option>';
					}
				} ?>
          </select>
          <?php /*?><script type="text/javascript">document.getElementById('types').value = "<?php echo set_value('type'); ?>"</script><?php */?>
        </div>
      </div>
      <div class="form-group">
        <label for="inputError" class="col-md-3 control-label" >Description</label>
        <div class="col-md-8">
          <textarea class="form-control" rows="5" id="description" name="description"><?php echo set_value('description'); ?></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="inputError" class="col-md-3 control-label" >Image</label>
        <div class="col-md-8">
          <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp">
        </div>
      </div>
      <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
        <button class="btn btn-sm btn-primary" type="submit">Save</button>
        <button class="btn btn-sm btn-primary" type="reset">Cancel</button>
      </div> </div>
      <?php echo form_close(); ?> </div>
  </div>
</div>
