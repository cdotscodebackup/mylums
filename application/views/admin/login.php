<!DOCTYPE html> 
<html lang="en-US">
  <head>
    <title>Lums Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    
    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>assets/css/oneui.css">
  </head>
  <body>
  	<div class="content overflow-hidden">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                    <!-- Login Block -->
                    <div class="block block-themed animated fadeIn">
                        <div class="block-header bg-primary">
                            <ul class="block-options">
                            </ul>
                            <h3 class="block-title">Login</h3>
                        </div>
                        <div class="block-content block-content-full block-content-narrow">
                            <!-- Login Title -->
                            <h1 class="h2 font-w600 push-30-t push-5">Lums Application</h1>
                            <p>Welcome, please login.</p>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                            <?php /*?><form class="" action="base_pages_dashboard.html" method="post"><?php */?>
							<?php 
							$attributes = array('class' => 'js-validation-login form-horizontal push-30-t push-50');
							echo form_open('admin/login/validate_credentials', $attributes);?>
                            	
                                <div class="form-group">
                                	<div class="col-xs-12">
                                    	<?php  if(isset($message_error) && $message_error){
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> Change a few things up and try submitting again.';
          echo '</div>';             
      } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="text" id="user_name" name="user_name">
                                            <label for="user_name">Email</label>
                                            <?php //echo form_input('user_name', '', 'placeholder="Username"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="password" id="password" name="password">
                                            <label for="password">Password</label>
                                            <?php //echo form_password('password', '', 'placeholder="Password"'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                	<div class="col-xs-12 col-sm-6 col-md-4">
                                        <button class="btn btn-block btn-primary" type="submit"><i class="si si-login pull-right"></i> Log in</button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Login Form -->
                        </div>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
        
    <!--container-->
   
    
    <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.appear.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.countTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/js.cookie.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    
    <!-- Page JS Plugins -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
    
    <!-- Page JS Code -->
    <script src="<?php echo base_url(); ?>assets/js/pages/base_pages_login.js"></script>
  </body>
</html>    
    