<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li> <a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li> <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"> <?php echo ucfirst($this->uri->segment(2));?> </a></li>
      <li class="active"> <a href="#">Update</a> </li>
    </ul>
    <div class="page-header">
      <h2> <?php echo ucfirst($this->uri->segment(2));?> Gallery  </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
    <div class="block">
    	<div class="block-header"> </div>
        <div class="block-header">
        	<?php
          //flash messages
          if($this->session->flashdata('flash_message')){
            if($this->session->flashdata('flash_message') == 'updated')
            {
              echo '<div class="alert alert-success">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Well done!</strong> '.ucfirst($this->uri->segment(2)).' updated with success.';
              echo '</div>';       
            }else{
              echo '<div class="alert alert-error">';
                echo '<a class="close" data-dismiss="alert">×</a>';
                echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
              echo '</div>';          
            }
          }
          ?>
    <?php
          //form data
          $attributes = array('class' => 'form-horizontal', 'id' => 'submitgallery','target' => 'galleryUpload');
    
          //form validation
          echo validation_errors();
          echo form_open_multipart('admin/'.$this->uri->segment(2).'/addgallery/'.$this->uri->segment(4).'', $attributes); ?>
          <h2>Images </h2>
          <input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp" onchange="document.getElementById('submitgallery').submit();">
           <div class="row">
          <?php if(count($images) > 0) { ?>
          <?php foreach($images as $image){ ?>
          	<?php if($image['type'] == 'photo') { ?>
              <div class="form-groups col-md-2">
                 <?php /*?><?php */?>
                  <?php 
                    if($image['path'])
                    {
                        echo '<img src="'.base_url().'uploads/'.$image['path'].'" width="100" />';
                    }?>
                	<a href="#." onclick="confirmDeletes('<?php echo site_url("admin").'/'.$this->uri->segment(2).'/gallery/'.$image['event_id'].'?del='.$image['id']; ?>')" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></a>
              </div>
      <?php 	}
	  		}
		}?>
        </div>
        <h2 style="margin-top:20px;">Videos</h2>
        <input type="text" class="form-control-file" id="InputFile" name="urls" aria-describedby="fileHelp" onchange="document.getElementById('submitgallery').submit();">
        <div class="row">
        
       <?php if(count($images) > 0) { ?>
          <?php foreach($images as $image){ ?>
          	<?php if($image['type'] == 'video')
			{ ?>
              <div class="col-md-3">
              <div class="form-groups">
                 <?php /*?><input type="file" class="form-control-file" id="InputFile" name="image" aria-describedby="fileHelp"><?php */?>
                   <iframe src="https://www.youtube.com/embed/<?php echo quickYouTubeId($image['path'])  ; ?>" width="100%" height="250" frameborder="0"></iframe>
                  <a href="#." onclick="confirmDeletes('<?php echo site_url("admin").'/'.$this->uri->segment(2).'/gallery/'.$image['event_id'].'?del='.$image['id']; ?>')" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></a>
                   </div>
                   </div>
      <?php 	}
	  		}
		}?> </div>
      <div class="block-header">  
      <div class="form-groups">
              <div class="col-md-8 col-md-offset-4">
        <button class="btn btn-primary" type="submit">Save changes</button>
        <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>"><button class="btn" type="button">Cancel</button></a>
      </div>
            </div>
            </div>
    <?php echo form_close(); ?>
    	</div>
    </div> 
</div>
<iframe id="galleryUpload" name="galleryUpload" style="display:none;"></iframe>
<?php //$this->load->view('map'); ?>
<?php
function quickYouTubeId($youtubeurl) {
    preg_match("#[a-zA-Z0-9-]{11}#", $youtubeurl, $id);
    return (strlen($id[0])==11) ? $id[0] : false;
}
?>
<script type="text/javascript">
	function pageLoad()
	{
		$('#submitgallery').load(document.URL +  ' #submitgallery');
	}
	
	function confirmDeletes(locations) 
	{ 
		//alert(locations);
		
		var status = confirm("Are you sure you want to delete?");   
		if(status)
		{
			//return false;
			//window.location = locations;
			$('#submitgallery').load(locations +  ' #submitgallery');
			//parent.location.replace("parent.location='<?php echo "delete.php?id=" . $people['id'] ?>'");
		}
	}
	
</script>