    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-right">
            Design and Developed by <strong><a href="https://www.creative-dots.com/" target="_blank">Creative Dots</a></strong>
        </div>
        <div class="pull-left">
             &copy; Lums
        </div>
    </footer>
    <!-- END Footer -->
</div>
    <!-- END Apps Modal -->
    
    <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
    <?php /*?>
	<script src="<?php echo base_url(); ?>assets/js/core/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.appear.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.countTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/core/js.cookie.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/timepicker/jquery-1.11.3.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/timepicker/timepicki.js"></script>
	<?php */?>
</body>
</html>